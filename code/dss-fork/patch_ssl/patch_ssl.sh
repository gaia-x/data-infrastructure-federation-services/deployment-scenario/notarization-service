#!/bin/bash

#default_IFS="$IFS"
#IFS="|"

pass=changeit
keystore=$(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts
crt=ssi.gouv.fr.crt

yes | keytool -storepass $pass -importcert -alias server -file $crt -keystore $keystore