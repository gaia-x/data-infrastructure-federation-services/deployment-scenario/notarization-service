#!/bin/sh
set -e

echo "`/bin/sh /dss/apache-tomcat-*/bin/startup.sh`"
#echo "`/bin/sh /dss/apache-tomcat-*/bin/catalina.sh run`"
exec "$@"

tail -f /dss/apache-tomcat-*/logs/catalina.out