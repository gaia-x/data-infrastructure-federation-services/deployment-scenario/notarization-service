package eu.europa.esig.dss.web.job;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import eu.europa.esig.dss.spi.x509.CertificateSource;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.web.config.DSSBeanConfig;

@Service
public class TRLoaderJob {

	private static final Logger LOG = LoggerFactory.getLogger(TRLoaderJob.class);

	@Value("${cron.tr.loader.enable}")
	private boolean enable;

	@Autowired
	@Qualifier("trusted-registry-certificate-verifier")
	private CertificateVerifier certificateVerifier;

	@Autowired
	private DSSBeanConfig dssBeanConfig;

	@PostConstruct
	public void init() {
		LOG.info("init Trusted Registry LoaderJob");
	}

	@Scheduled(initialDelayString = "${cron.initial.delay.tr.loader}", fixedDelayString = "${cron.delay.tr.loader}")
	public void refresh() {
		if (enable) {
			LOG.info("refresh Trusted Registry LoaderJob");
			CertificateSource cs = dssBeanConfig.gaiaxCertSource();
			certificateVerifier.addTrustedCertSources(cs);
		}
	}

}
