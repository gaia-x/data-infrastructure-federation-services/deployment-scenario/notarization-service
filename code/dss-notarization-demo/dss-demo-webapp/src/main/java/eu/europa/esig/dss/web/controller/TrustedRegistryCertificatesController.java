package eu.europa.esig.dss.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eu.europa.esig.dss.spi.x509.CertificateSource;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.web.config.DSSBeanConfig;
import eu.europa.esig.dss.web.service.KeystoreService;

@Controller
@RequestMapping(value = "/tr-certificates" )
public class TrustedRegistryCertificatesController {

	private static final String CERTIFICATE_TILE = "tr-certificates";

	private static final String[] ALLOWED_FIELDS = { }; // nothing

	@InitBinder
	public void setAllowedFields(WebDataBinder webDataBinder) {
		webDataBinder.setAllowedFields(ALLOWED_FIELDS);
	}
	
	@Autowired
	private DSSBeanConfig dssBeanConfig;

	@Autowired
	@Qualifier("trusted-registry-certificate-verifier")
	private CertificateVerifier certificateVerifier;

	//@Autowired
	//@Qualifier("trusted-registry-certificate-source")
	//private CertificateSource certificateSource;

	@Autowired
	private KeystoreService keystoreService;

	@RequestMapping(method = RequestMethod.GET)
	public String showCertificates(Model model, HttpServletRequest request) {		
		CertificateSource cs = dssBeanConfig.gaiaxCertSource();
		model.addAttribute("keystoreCertificates", keystoreService.getCertificatesDTOFromKeyStore(cs.getCertificates()));
		
		//Set<CertificateToken> sct = certificateVerifier.getTrustedCertSources().getAllCertificateTokens();
		//List<CertificateToken> lct = new ArrayList<>(sct);
		//model.addAttribute("keystoreCertificates", keystoreService.getCertificatesDTOFromKeyStore(lct));
		
		return CERTIFICATE_TILE;
	}

}