Notarization-api
=============

Setup
-----

- Install Python 3 and git.
- Run `setup.sh`
- Run `./server.py` to start the server
- Open `http://localhost:5000/` on your web browser to run the client

