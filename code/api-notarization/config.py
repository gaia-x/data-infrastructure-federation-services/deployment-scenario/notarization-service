import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

LOCAL_ENV = os.getenv("LOCAL_ENV", False)#.replace('\n', '')

API_KEY_AUTHORIZED = os.getenv('API_KEY_AUTHORIZED', 'dummy-key').replace('\n','')

LOG_FILE_NAME = "/tmp/notarization.log"

FLASK_PORT = 5000
FLASK_HOST = "0.0.0.0"
FLASK_DEBUG = True
