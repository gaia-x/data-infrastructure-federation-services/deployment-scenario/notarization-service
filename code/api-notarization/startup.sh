#!/bin/sh
set -e

echo "`python3 app.py`"
exec "$@"
