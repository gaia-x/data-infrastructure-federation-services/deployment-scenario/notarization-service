import json, requests, logging

#logging.basicConfig(level=logging.WARNING)
FEDERATED_CATALOGUE_URL     = "https://federated-catalogue-api.abc-federation.gaia-x.community/api"
COMPLIANCE_REFERENCES_URL   = f"{FEDERATED_CATALOGUE_URL}/compliance_references"


def get_all_compliance_references_by_name():       
    references_by_name = {}
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Accept": "application/json, application/javascript, text/javascript, text/json"
    }
    ack, res = get_json_from_url(url=COMPLIANCE_REFERENCES_URL, data=None, headers=headers)
    
    if ack:
        for r in res:
            did = r.get('compliance_ref_did')
            title = r.get('title')
            version = r.get('version')
            name = f"{title} {version}" if version else f"{title}"
            references_by_name[name] = did
    else:
        logging.error(res)
    
    return references_by_name
    

def convert_did_to_https(did):
    return f"https://{did.replace('did:web:', '').replace(':', '/')}"

def get_json_from_url(url, data=None, headers={}):
    js = None
    try:
        response = requests.get(url=url, data=data, headers=headers)
        if response.status_code == 200:
            js = response.json()
        else:
            return False, f"Error HTTP Code {response.status_code} with {url} : {response.text}"
    except Exception as err:
        return False, f"Error with {url}: {err}, {type(err)}"
    
    return True, js



if __name__ == "__main__":
    n = 0
    refs = get_all_compliance_references_by_name()
    
    for ref_name in list(refs.keys()):
        n += 1
        print(f"{n} {ref_name}")
        ref_did = refs[ref_name]
        ref_url = convert_did_to_https(ref_did)
        ack, ref = get_json_from_url(ref_url)
        if not ack:
            logging.error(f"Error with {ref_url}: {ref}")
            break
        
        #Get Schemes from ComplianceReference
        schemes = ref.get('gax-compliance:hasComplianceCertificationSchemes')
        if not schemes:
            #logging.error(f"Error with {ref}: No schemes")
            break
        
        scheme, scheme_did = None, None
        #Check if federation is included in cab of the scheme of the reference
        for s in schemes:
            scheme_tmp_did = s.get("@id")
            scheme_tmp_url = convert_did_to_https(scheme_tmp_did)
            ack, scheme_tmp = get_json_from_url(scheme_tmp_url)
            if not ack:
                #logging.error(f"Error with {scheme_tmp_url}: {scheme_tmp}")
                break
            
            cabs = scheme_tmp.get('gax-compliance:hasComplianceAssessmentBodies')
            if not cabs:
                #logging.error(f"Error with {scheme_tmp_url}: No cabs")
                break
            
            for cab in cabs:
                cab_did = cab.get('@id')
                if "abc-federation" in cab_did:
                    scheme, scheme_did = scheme_tmp, scheme_tmp_did
                    break
        #if federetion is not, federation is not allowed to issue claim of given certification
        #print(f"{ref_title} {ref_version if ref_version else ''}: {'Allowed' if scheme else 'Not allowed'} ")
        print(f"{ref_name} : {'Allowed' if scheme else 'Not Allowed'}")
    exit(-1)
    abc_crm_url = "https://abc-federation.gaia-x.community/participant/8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/data.json"
    abc_crm = get_json_from_url(abc_crm_url)

    if not abc_crm:
        print(f"No compliance reference manager for given url {abc_crm_url}")
        exit(-1)
        
    cr_list = abc_crm.get('gax-compliance:hasComplianceReferences')

    for cr in cr_list:
        cr_did = cr.get("@id")
        cr_url = convert_did_to_https(cr_did)
        js = get_json_from_url(cr_url)
        title, version = None, None
        if js.get('gax-compliance:hasComplianceReferenceTitle'):
            if js.get('gax-compliance:hasComplianceReferenceTitle').get('@value'):
                title = js.get('gax-compliance:hasComplianceReferenceTitle').get('@value')
        if js.get('gax-compliance:hasVersion'):
            if js.get('gax-compliance:hasVersion').get('@value'):
                version = js.get('gax-compliance:hasVersion').get('@value')
        if title and version:
            print(f'<option value="{title} {version}">{title} {version}</option>')
        elif title or version:
            print(f'<option value="{title}">{title}</option>')
        #print(f" > {title} - {version}: {cr_did}")
    