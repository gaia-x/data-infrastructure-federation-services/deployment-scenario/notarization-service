import re, logging
from flask import Flask, Blueprint, render_template, request
from flask_cors import cross_origin
from .variables import *
from .ontology import *
from config import *

api = Blueprint('api_v2206', __name__)
logging.basicConfig(filename=LOG_FILE_NAME, level=logging.DEBUG)

# # # #
# GET /api/, return index page
# # # #
@api.route('/', methods=['GET'])
@cross_origin()
def index():
  logging.info(f"{request.method} on /")
  return render_template(f'{VERSION}/index.html')

# # # #
# GET /api/status, return status of notarization service
#   no args
# # # #
@api.route(rule='/status', methods=['GET'])
@cross_origin()
def status():
  return format_response("success", "ok", 200)

# # # #
# GET /api/upload, return page for uploading pdf and generate VC
#   no args
# # # #
@api.route(rule='/upload_<int:nb>', methods=['GET'])
@cross_origin()
def upload(nb):
    logging.info(f"{request.method} on /upload_{nb}")
    return render_template(f'{VERSION}/upload_{nb}.html')

# # # #
# POST /api/create-vp, return VP of VC sent
#   args = json {
#           "participant": <string>,
#           "locatedServiceOffering": <json>,
#           "questionnaire": <json>
#       }
# # # #
@api.route(rule='/create-vp', methods=['POST'])
@cross_origin()
def create_vp():
    #Inputs check
    json_obj = request.get_json()

    #Checking Participant
    if FORM_PARTICIPANT not in json_obj:
        return format_response("error", "No participant", 400)
    participant = json_obj.get(FORM_PARTICIPANT)
    try:
        if participant == '':
            raise
    except:
        return format_response("error", "Participant invalid", 400)

    #Checking Service Offering
    if FORM_LOCATED_SERVICE_OFFERING not in json_obj:
        return format_response("error", "No located-service-offering", 400)
    locatedServiceOffering = json_obj.get(FORM_LOCATED_SERVICE_OFFERING)
    if not locatedServiceOffering:
        return format_response("error", "LocatedServiceOffering error", 400)
    
    #Checking Service Offering
    if FORM_QUESTIONNAIRE not in json_obj:
        return format_response("error", "No questionnaire", 400)
    questionnaire = json_obj.get(FORM_QUESTIONNAIRE)
    if not questionnaire:
        return format_response("error", "Questionnaire error", 400)

    dufour_gen = DufourGenerator()

    #Create n ComplianceCriterion
    crit_list = []
    for criteria in questionnaire:
        txt = criteria.get('criterion')
        n = extract_criterion_number(txt)
        val = criteria.get('val')
        if val:
            crit_list.append(LOCAL_CRITERIONS[n])
    
    #Create SelfClaim //id, api, did, issuer, criterions
    selfclaim = SelfAssessedComplianceCriteriaClaim(dufour_gen.selfclaim_id, dufour_gen.selfclaim_api, dufour_gen.selfclaim_did,
                    DUFOURSTORAGE_DID, crit_list, locatedServiceOffering.get("@id"))

    #Create SelfCredential //id, api, did, issuer, selfclaim
    selfcredential = SelfAssessedComplianceCriteriaCredential(dufour_gen.selfcred_id, dufour_gen.selfcred_api, dufour_gen.selfcred_did,
                    DUFOURSTORAGE_DID, dufour_gen.selfclaim_did)

    locatedUpdate = LocatedServiceOfferingUpdate(locatedServiceOffering)
    locatedUpdate.add_claim(selfclaim.get_did(), type="SelfAssessedComplianceCriteriaClaim")

    #Create VP
    vp = VerifiablePresentation(dufour_gen.vp_id, dufour_gen.vp_api, dufour_gen.vp_did,
                    DUFOURSTORAGE_DID, locatedServiceOffering)
    vp_unsigned = vp.get_template()

    if LOCAL_ENV:
        #Write object to files
        vp.write_to_file("./gen")
        selfclaim.write_to_file("./gen")
        selfcredential.write_to_file("./gen")
        locatedUpdate.write_to_file('./gen')
    else:
        #Store all objects
        #vp.store_to_agent()
        selfclaim.store_to_agent()
        selfcredential.store_to_agent()
        locatedUpdate.store_to_agent()

    return format_response("success", "VP unsigned generated", 200, None, vp_unsigned)

# # # #
# POST /api/create-vc, return VC if pdf is well signed
#   args = json {
#           "participant": <string>,
#           "certificateType": <string>,
#           "b64file": <string>,
#           "serviceOffering": <json>,
#                    OR
#           "locatedServiceOffering": <json>
#       }
# # # #
@api.route(rule='/create-vc', methods=['POST'])
@cross_origin()
def create_vc():
    #Inputs check
    json_obj = request.get_json()

    #Checking Participant
    if FORM_PARTICIPANT not in json_obj:
        return format_response("error", "No participant", 400)
    participant = json_obj.get(FORM_PARTICIPANT)
    try:
        if participant == '':
            raise
    except:
        return format_response("error", "Participant invalid", 400)

    #Checking Certification Type
    if FORM_CERTIFICATION_TYPE not in json_obj:
        return format_response("error", "No certification type", 400)
    certificationType = json_obj.get(FORM_CERTIFICATION_TYPE)
    if certificationType not in ALLOWED_CLAIMS:
        return format_response("error", "Certification type not allowed", 400)

    #Check B64File (take it as is)
    if FORM_B64FILE not in json_obj:
        return format_response("error", "No b64 file", 400)
    signed_pdf_b64 = json_obj.get(FORM_B64FILE).encode('UTF-8')

    #Checking Service Offering
    serviceOffering = None
    if FORM_SERVICE_OFFERING in json_obj:
        serviceOffering = json_obj.get(FORM_SERVICE_OFFERING)
        if not serviceOffering:
            return format_response("error", "ServiceOffering error", 400)

    #Checking Located Service Offering
    locatedServiceOffering = None
    if FORM_LOCATED_SERVICE_OFFERING in json_obj:
        locatedServiceOffering = json_obj.get(FORM_LOCATED_SERVICE_OFFERING)
        if not locatedServiceOffering:
            return format_response("error", "No located-service-offering", 400)
    
    #Check signature with DSS-Notarization
    isChecked, result, message, status = check_signature(b64encoded_doc_signed=signed_pdf_b64, 
                doc_signed_name="document")
    
    if not isChecked:
        return format_response(result, message, status)

    if locatedServiceOffering:
        vc, vp = create_vc_update(certificationType, locatedServiceOffering)
    else:
        vc, vp = create_vc_new(certificationType, serviceOffering)
    
    vc_signed = vc.get_template_signed()
    vp_signed = None
    #vp_signed = vp.get_template_signed()

    return format_response(result, message, status, vc_signed, vp_signed)

# # # #
# FCT, return VC
# # # #
def create_vc_new(certificationType, serviceOffering):
    dufour_gen = DufourGenerator()
    fede_gen = FederationGenerator()
    
    #Recreate ServiceOffering
    so = ServiceOffering(DUFOURSTORAGE_DID, serviceOffering)
    
    #Create Scheme
    compliance_reference_did = get_compliance_reference_did(certificationType)
    scheme = ThirdPartyComplianceCertificationScheme(fede_gen.scheme_id, fede_gen.scheme_api, fede_gen.scheme_did,
                    FEDERATION_DID, compliance_reference_did, get_criterions_by_certification(certificationType), [fede_gen.cab_did])

    #Create CAB
    cab = ComplianceAssessmentBody(fede_gen.cab_id, fede_gen.cab_api, fede_gen.cab_did, 
                    FEDERATION_DID, fede_gen.scheme_did, fede_gen.claim_did, fede_gen.cred_did)

    #Create Claim
    claim = ThirdPartyComplianceCertificateClaim(fede_gen.claim_id, fede_gen.claim_api, fede_gen.claim_did,
                    FEDERATION_DID, fede_gen.scheme_did, dufour_gen.located_did, fede_gen.cab_did)

    #Create Credential
    credential = ThirdPartyComplianceCertificateCredential(fede_gen.cred_id, fede_gen.cred_api, fede_gen.cred_did,
                    FEDERATION_DID, fede_gen.claim_did)
    
    #Create LocatedServiceOffering
    service_offering_did = serviceOffering.get('@id')
    if is_credential(serviceOffering):
        location_did = serviceOffering.get('credentialSubject').get('gax-service:isAvailableOn')[0].get('@id')
    else:
        location_did = serviceOffering.get('gax-service:isAvailableOn')[0].get('@id')
    located = LocatedServiceOffering(dufour_gen.located_id, dufour_gen.located_api, dufour_gen.located_did,
                    DUFOURSTORAGE_DID, service_offering_did, [location_did], [fede_gen.claim_did])

    #Create VP
    vp = None
    #vp = VerifiablePresentation(dufour_gen.vp_id, dufour_gen.vp_api, dufour_gen.vp_did,
    #                DUFOURSTORAGE_DID, located.get_template_signed())

    if LOCAL_ENV:
        #Write object to files
        so.write_to_file("./gen")
        scheme.write_to_file("./gen")
        cab.write_to_file("./gen/")
        claim.write_to_file("./gen")
        credential.write_to_file("./gen")
        located.write_to_file("./gen")
        #vp.write_to_file("./gen")
    else:
        #Store all objects
        so.store_to_agent()
        scheme.store_to_agent()
        cab.store_to_agent()
        claim.store_to_agent()
        credential.store_to_agent()
        located.store_to_agent()
        #vp.store_to_agent()

    return located, vp

# # # #
# FCT, return VC after update a VC given in arg
# # # #
def create_vc_update(certificationType, locatedServiceOffering):
    dufour_gen = DufourGenerator()
    fede_gen = FederationGenerator()
    
    #Create Scheme
    compliance_reference_did = get_compliance_reference_did(certificationType)
    scheme = ThirdPartyComplianceCertificationScheme(fede_gen.scheme_id, fede_gen.scheme_api, fede_gen.scheme_did,
                    FEDERATION_DID, compliance_reference_did, get_criterions_by_certification(certificationType), [fede_gen.cab_did])
    
    #Create CAB
    cab = ComplianceAssessmentBody(fede_gen.cab_id, fede_gen.cab_api, fede_gen.cab_did, 
                    FEDERATION_DID, fede_gen.scheme_did, fede_gen.claim_did, fede_gen.cred_did)
    
    #Create Claim
    claim = ThirdPartyComplianceCertificateClaim(fede_gen.claim_id, fede_gen.claim_api, fede_gen.claim_did,
                    FEDERATION_DID, fede_gen.scheme_did, dufour_gen.located_did, fede_gen.cab_did)
    
    #Create Credential
    credential = ThirdPartyComplianceCertificateCredential(fede_gen.cred_id, fede_gen.cred_api, fede_gen.cred_did,
                    FEDERATION_DID, fede_gen.claim_did)
    
    #Update LocatedServiceOffering
    locatedUpdate = LocatedServiceOfferingUpdate(locatedServiceOffering)
    locatedUpdate.add_claim(claim.get_did(), type="ThirdPartyComplianceCertificateClaim")
    #locatedUpdate.add_credential(credential.get_did())
    
    #Create VP
    #vp = VerifiablePresentation(dufour_gen.vp_id, dufour_gen.vp_api, dufour_gen.vp_did,
    #                DUFOURSTORAGE_DID, locatedUpdate.get_template_signed())
    vp = None

    if LOCAL_ENV:
        #Write object to files
        scheme.write_to_file("./gen")
        cab.write_to_file("./gen/")
        claim.write_to_file("./gen")
        credential.write_to_file("./gen")
        locatedUpdate.write_to_file("./gen")
        #vp.write_to_file("./gen")
    else:
        #Store all objects
        scheme.store_to_agent()
        cab.store_to_agent()
        claim.store_to_agent()
        credential.store_to_agent()
        locatedUpdate.store_to_agent()
        #vp.store_to_agent()

    return locatedUpdate, None #vp

# # # #
# FCT, verify signature
# # # #
def check_signature(b64encoded_doc_signed, doc_signed_name, b64encoded_doc_origin=None, doc_origin_name=None):
    return True, "success", "Valid signature", 200

    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Accept": "application/json, application/javascript, text/javascript, text/json"
    }
    data = {
        "signedDocument" : {
            "bytes" : b64encoded_doc_signed.decode('UTF-8'),
            "digestAlgorithm" : None,
            "name" : doc_signed_name
        },
        "policy" : None,
        "tokenExtractionStrategy" : "NONE",
        "signatureId" : None
    }
    if b64encoded_doc_origin:
        data["originalDocuments"] = [ {
            "bytes" : b64encoded_doc_origin.decode('UTF-8'),
            "digestAlgorithm" : None,
            "name" : doc_origin_name
        } ]
    
    response = requests.post(DSS_NOTARIZATION_URL, data=json.dumps(data), headers=headers)

    if response.status_code == 200:
        response = json.loads(response.text)
        simpleReport = response["SimpleReport"]
        
        if simpleReport['ValidSignaturesCount'] > 0:
            return True, "success", "Valid signature", 200
        else:
            return False, "error", "No valid signature fund", 400
    else:
        return False, "error", f"Error HTTP Code {response.status_code} with {DSS_NOTARIZATION_URL} : {response.text}", 400

# # # #
# FCT, verify if file extension is allowed
# # # #
def allowed_file_extension(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# # # #
# FCT, format response to service rest
# # # #
def format_response(result, message, status, vc="", vp=""):
    if result == "error":
        logging.error(message)
    elif result == "success":
        logging.info(message)
    
    return Flask.response_class(
        response=json.dumps({"result": result, "message": message, "vc": vc, "vp": vp}),
        status=status,
        mimetype="aplication/json"
    )

# # # #
# FCT, return ComplianceReference object with name given in arg
# # # #
def get_compliance_reference_did(name):
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Accept": "application/json, application/javascript, text/javascript, text/json"
    }
    try:
        response = requests.get(COMPLIANCE_REFERENCES_URL, data=None, headers=headers)
        if response.status_code == 200:
            res = json.loads(response.text)
            for o in res:
                if name == o.get('title'):
                    return o.get('compliance_ref_did')
            logging.error(f"No compliance_references with title equal to {name} fund in catalogue...")
            return None
        else:
            logging.error(f"Error HTTP Code {response.status_code} with {COMPLIANCE_REFERENCES_URL} : {response.text}")
    except Exception as err:
        logging.error(f"Error with compliance_references: {err}, {type(err)}")
    return None


def get_all_criterions():
    criterions = {}
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Accept": "application/json, application/javascript, text/javascript, text/json"
    }
    try:
        response = requests.get(COMPLIANCE_CRITERIONS_URL, data=None, headers=headers)
        if response.status_code == 200:
            res = json.loads(response.text)
            cs = res.get('objects')
            for c in cs:
                did = c.get('@id')
                txt = c.get('gax-compliance:hasName').get('@value')
                n = extract_criterion_number(txt)
                criterions[n] = did
        else:
            logging.error(f"Error HTTP Code {response.status_code} with {COMPLIANCE_REFERENCES_URL} : {response.text}")
    except Exception as err:
        logging.error(f"Error with compliance_references: {err}, {type(err)}")
    return criterions

def get_all_local_criterions():
    return LOCAL_CRITERIONS

def get_criterions_by_certification(certification_type):
    try:
        return [LOCAL_CRITERIONS[d] for d in CERTIF_CRIT_LINK[certification_type]]
    except:
        return []

def extract_criterion_number(txt):
    try:
        return int(re.search('[0-9]+', txt).group())
    except:
        return None

