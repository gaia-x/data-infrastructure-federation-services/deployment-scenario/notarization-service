import hashlib, string, random, json, os, requests
from .variables import *
from config import API_KEY_AUTHORIZED

#Variables :
FEDERATION_NAME             = "abc-federation"
FEDERATION_API              = f"https://{FEDERATION_NAME}.gaia-x.community/api/store_object/"
FEDERATION_ID               = hashlib.sha256(bytes(FEDERATION_NAME, 'utf-8')).hexdigest()
FEDERATION_DID              = f"did:web:{FEDERATION_NAME}.gaia-x.community"
FEDERATION_DID_PREFIX       = f"{FEDERATION_DID}:participant:{FEDERATION_ID}"
FEDERATION_VC_ISSUER_URL    = f"https://vc-issuer.{FEDERATION_NAME}.gaia-x.community/api/v0.9/sign"

DUFOURSTORAGE_NAME          = "dufourstorage"
DUFOURSTORAGE_API           = f"https://{DUFOURSTORAGE_NAME}.provider.gaia-x.community/api/store_object/"
DUFOURSTORAGE_ID            = hashlib.sha256(bytes(DUFOURSTORAGE_NAME, 'utf-8')).hexdigest()
DUFOURSTORAGE_DID           = f"did:web:{DUFOURSTORAGE_NAME}.provider.gaia-x.community"
DUFOURSTORAGE_DID_PREFIX    = f"{DUFOURSTORAGE_DID}:participant:{DUFOURSTORAGE_ID}"
DUFOURSTORAGE_VC_ISSUER_URL = f"https://vc-issuer.{DUFOURSTORAGE_NAME}.provider.gaia-x.community/api/v0.9/sign"

class ComplianceCreterionGenerator():
    def __init__(self, number=60) -> None:
        self.criterions = []
        for i in range(number):
            self.criterions.append(ComplianceCriterion(i))
    
    def get_criterion(self, i):
        return self.criterions.get(i)

class ComplianceCriterion():
    def __init__(self, index) -> None:
        self.id = hashlib.sha256(bytes(f"criterion_{index}")).hexdigest()
        self.api = 'compliance-criterion'
        self.did = f"{FEDERATION_DID_PREFIX}/{self.api}/{self.id}/data.json"
        self.path = f'{self.api}/{id}'
    
    def get_id(self):
        return self.id

    def get_api(self):
        return self.api
    
    def get_did(self):
        return self.did

    def get_path(self):
        return self.path


class FederationGenerator():
    def __init__(self) -> None:
        self.scheme_id  = hashlib.sha256(bytes(''.join(random.choices(string.ascii_lowercase, k=10)), 'utf-8')).hexdigest()
        self.scheme_api = 'third-party-compliance-certification-scheme'
        self.scheme_did = f"{FEDERATION_DID_PREFIX}/{self.scheme_api}/{self.scheme_id}/data.json"

        self.cab_id  = hashlib.sha256(bytes(''.join(random.choices(string.ascii_lowercase, k=10)), 'utf-8')).hexdigest()
        self.cab_api = "compliance-assessment-body"
        self.cab_did = f"{FEDERATION_DID_PREFIX}/{self.cab_api}/{self.cab_id}/data.json"

        self.claim_id  = hashlib.sha256(bytes(''.join(random.choices(string.ascii_lowercase, k=10)), 'utf-8')).hexdigest()
        self.claim_api = 'third-party-compliance-certificate-claim'
        self.claim_did = f"{FEDERATION_DID_PREFIX}/{self.claim_api}/{self.claim_id}/data.json"

        self.cred_id  = hashlib.sha256(bytes(''.join(random.choices(string.ascii_lowercase, k=10)), 'utf-8')).hexdigest()
        self.cred_api = 'third-party-compliance-certificate-credential'
        self.cred_did = f"{FEDERATION_DID_PREFIX}/{self.cred_api}/{self.cred_id}/data.json"

class DufourGenerator():
    def __init__(self) -> None:
        self.located_id  = hashlib.sha256(bytes(''.join(random.choices(string.ascii_lowercase, k=10)), 'utf-8')).hexdigest()
        self.located_api = 'located-service-offering'
        self.located_did = f"{DUFOURSTORAGE_DID_PREFIX}/{self.located_api}/{self.located_id}/data.json"

        self.vc_id  = hashlib.sha256(bytes(''.join(random.choices(string.ascii_lowercase, k=10)), 'utf-8')).hexdigest()
        self.vc_api = 'vc'
        self.vc_did = f"{DUFOURSTORAGE_DID_PREFIX}/{self.vc_api}/{self.vc_id}/data.json"

        self.vp_id  = hashlib.sha256(bytes(''.join(random.choices(string.ascii_lowercase, k=10)), 'utf-8')).hexdigest()
        self.vp_api = 'vp'
        self.vp_did = f"{DUFOURSTORAGE_DID_PREFIX}/{self.vp_api}/{self.vp_id}/data.json"

        self.selfclaim_id  = hashlib.sha256(bytes(''.join(random.choices(string.ascii_lowercase, k=10)), 'utf-8')).hexdigest()
        self.selfclaim_api = 'self-assessed-compliance-criteria-claim'
        self.selfclaim_did = f"{DUFOURSTORAGE_DID_PREFIX}/{self.selfclaim_api}/{self.selfclaim_id}/data.json"

        self.selfcred_id  = hashlib.sha256(bytes(''.join(random.choices(string.ascii_lowercase, k=10)), 'utf-8')).hexdigest()
        self.selfcred_api = 'self-assessed-compliance-criteria-credential'
        self.selfcred_did = f"{DUFOURSTORAGE_DID_PREFIX}/{self.selfcred_api}/{self.selfcred_id}/data.json"

# # # #
# Abstract Class for credential, composed with several s atributes :
#  - id : string (sha-256 of a random string)
#          "0106b44dae1c2cfe5e911a5c54eb10f434d38a4a64874447b7ecf4ce1b695d95"
#  - did : string (https links of resolvable did) 
#           "https://dufourstorage.provider.gaia-x.community/participant/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/located-service-offering/0106b44dae1c2cfe5e911a5c54eb10f434d38a4a64874447b7ecf4ce1b695d95/data.json"
#  - api : string (type of object)  
#           "located-service-offering"
#  - path : string (path of object)
#            "located-service-offering/0106b44dae1c2cfe5e911a5c54eb10f434d38a4a64874447b7ecf4ce1b695d95"
#  - template : json (json object)
#  - template_signed : json (json object with signature alias proof)
# # # #
class AbstractCredential():
    def __init__(self, id, api, did, issuer, template, template_signed) -> None:
        self.id = id
        self.did = did
        self.api = api
        self.path = f'{self.api}/{id}'
        self.template = template
        self.template_signed = template_signed
        self.issuer = issuer
        self.vc_issuer_url = DUFOURSTORAGE_VC_ISSUER_URL if DUFOURSTORAGE_NAME in issuer else FEDERATION_VC_ISSUER_URL
        self.agent_api = DUFOURSTORAGE_API if DUFOURSTORAGE_NAME in issuer else FEDERATION_API
    
    def get_id(self):
        return self.id
    
    def get_did(self):
        return self.did

    def get_template(self):
        return self.template

    def get_template_signed(self):
        if self.template_signed:
            return self.template_signed
        headers = {
                "Content-Type": "application/json; charset=UTF-8",
                "Accept": "application/json, application/javascript, text/javascript, text/json"
            }
        response = requests.put(self.vc_issuer_url, data=json.dumps(self.template), headers=headers)
        if response.status_code == 200:
            self.template_signed = json.loads(response.text)
            return self.template_signed
        else:
            print(f"Error HTTP Code {response.status_code} with {self.vc_issuer_url} for {self.api} : {response.text}")
            return None

    def write_to_file(self, rac='.'):
        template_signed = self.get_template_signed()
        os.makedirs(f'{rac}/{self.path}', exist_ok=True)
        with open(f'{rac}/{self.path}/data.json', 'w') as f:
            json.dump(obj=template_signed, fp=f, indent=4)

    def store_to_agent(self):
        print(f'Store {self.api} with id {self.did} on {self.agent_api}')
        headers = get_header_with_api_key()
        js = {'objectjson': json.dumps(self.get_template_signed())}
        response = requests.post(self.agent_api+self.api, headers=headers, json=js)
        print(f'{response.status_code}: {response.text}')
        print(f'***')
        return response.text

def get_header_with_api_key():    
    #b64_key = API_KEY_AUTHORIZED
    #key = base64.standard_b64decode(b64_key).decode('UTF-8').replace('\n','')
    key = API_KEY_AUTHORIZED
    return {'x-api-key': key}

class ThirdPartyComplianceCertificationScheme(AbstractCredential):
    def __init__(self, id, api, did, issuer, reference, criterions, cabs) -> None:
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "ThirdPartyComplianceCertificationScheme"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@type": "ThirdPartyComplianceCertificationScheme",
                    "@context": {
                        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                        "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
                    },
                    "gax-compliance:hasComplianceAssessmentBodies": [],
                    "gax-compliance:hasComplianceReference": {
                        "@type": "gax-compliance:ComplianceReference",
                        "@value": f"{reference}"
                    },
                    "gax-compliance:grantsComplianceCriteria": []
                }
            }
        for criterion in criterions:
            template.get('credentialSubject').get('gax-compliance:grantsComplianceCriteria').append(
                {
                    "@type": "gax-participant:ComplianceCriterion",
                    "@value": f"{criterion}"
                }
            )
        for cab in cabs:
            template.get('credentialSubject').get('gax-compliance:hasComplianceAssessmentBodies').append(
                {
                    "@type": "gax-participant:ComplianceAssessmentBody",
                    "@value": f"{cab}"
                }
            )
        template_signed = None
        super().__init__(id, api, did, issuer, template, template_signed)
        
class ComplianceAssessmentBody(AbstractCredential):
    def __init__(self, id, api, did, issuer, scheme, claim, credential) -> None:
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "ComplianceAssessmentBody"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@type": "ComplianceAssessmentBody",
                    "@context": {
                        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                        "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
                    },
                    "gax-compliance:canCertifyThirdPartyComplianceCertificationScheme": [{
                            "@type": "gax-compliance:ThirdPartyComplianceCertificationScheme",
                            "@value": f"{scheme}"
                        }
                    ],
                    "gax-compliance:hasThirdPartyComplianceCertificateClaim": [{
                            "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim",
                            "@value": f"{claim}"
                        }
                    ],
                    "gax-compliance:hasThirdPartyComplianceCredential": [{
                            "@type": "gax-compliance:ThirdPartyComplianceCertificateCredential",
                            "@value": f"{credential}"
                        }
                    ]
                }
            }
        template_signed = None
        super().__init__(id, api, did, issuer, template, template_signed)

class ThirdPartyComplianceCertificateClaim(AbstractCredential):
    def __init__(self, id, api, did, issuer, scheme, located, cab) -> None:
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "ThirdPartyComplianceCertificateClaim"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@type": "ThirdPartyComplianceCertificateClaim",
                    "@context": {
                        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                        "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
                        "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
                    },
                    "gax-compliance:hasComplianceCertificationScheme": {
                        "@type": "gax-compliance:ComplianceCertificationScheme",
                        "@value": f"{scheme}"
                    },
                    "gax-compliance:hasLocatedServiceOffering": {
                        "@type": "gax-service:LocatedServiceOffering",
                        "@value": f"{located}"
                    },
                    "gax-compliance:hasComplianceAssessmentBody": {
                        "@type": "gax-participant:ComplianceAssessmentBody",
                        "@value": f"{cab}"
                    }
                }
            }
        template_signed = None
        super().__init__(id, api, did, issuer, template, template_signed)

class ThirdPartyComplianceCertificateCredential(AbstractCredential):
    def __init__(self, id, api, did, issuer, claim) -> None:
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "ThirdPartyComplianceCertificateCredential"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@type": "ThirdPartyComplianceCertificateCredential",
                    "@context": {
                        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                        "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
                    },
                    "@id": f"{claim}",
                    "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim",
                    "isValid": {
                        "@value": "True"
                    }
                },
            }
        template_signed = None
        super().__init__(id, api, did, issuer, template, template_signed)

class LocatedServiceOffering(AbstractCredential): 
    def __init__(self, id, api, did, issuer, service_offering, locations, claims) -> None:
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "LocatedServiceOffering"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@type": "LocatedServiceOffering",
                    "@context": {
                        "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
                    },
                    "gax-service:isImplementationOf": {
                        "@id": f"{service_offering}",
                        "@type": "gax-service:ServiceOffering"
                    },
                    "gax-service:isHostedOn": [],
                    "gax-service:hasComplianceCertificateClaim": []
                }
            }
        for location in locations:
            template.get('credentialSubject').get('gax-service:isHostedOn').append(
                {
                    "@id": f"{location}",
                    "@type": "gax-participant:Location"
                }
            )
        for claim in claims:
            template.get('credentialSubject').get('gax-service:hasComplianceCertificateClaim').append(
                {
                    "@id": f"{claim}",
                    "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim"
                }
            )
        template_signed = None
        super().__init__(id, api, did, issuer, template, template_signed)

class LocatedServiceOfferingUpdate(AbstractCredential): 
    def __init__(self, template) -> None:
        id = template.get("@id").split('/')[-2]
        api = 'located-service-offering'
        did = template.get("@id")
        issuer = template.get('issuer')
        template_signed = None
        super().__init__(id, api, did, issuer, template, template_signed)
    
    def add_location(self, location):
        template = {
                "@id": f"{location}",
                "@type": "gax-participant:Location"
            }
        self.template.get('credentialSubject').get('gax-service:isHostedOn').append(template)

    def add_claim(self, claim, type):
        template = {
                "@id": f"{claim}",
                "@type": f"gax-compliance:{type}"
            }
        self.template.get('credentialSubject').get('gax-service:hasComplianceCertificateClaim').append(template)
    
    def add_credential(self, credential):
        template = {
                "@id": f"{credential}",
                "@type": "gax-compliance:ComplianceCertificateCredential"
            }
        self.template.get('credentialSubject').get('gax-service:hasComplianceCertificateCredential').append(template)

class VerifiablePresentation(AbstractCredential):
    def __init__(self, id, api, did, issuer, vc_signed) -> None:
        template = {
                "@context":[
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "@type": [
                    "VerifiablePresentation"
                ],
                "verifiableCredential": [
                    vc_signed
                ]
            }
        template_signed = None
        super().__init__(id, api, did, issuer, template, template_signed)

class ServiceOffering(AbstractCredential): 
    def __init__(self, issuer, template) -> None:
        id = template.get("@id").split('/')[-2]
        api = 'service-offering'
        did = template.get("@id")
        context = {
            "dct": "http://purl.org/dc/terms/",
            "dcat": "http://www.w3.org/ns/dcat#",
            "xsd": "http://www.w3.org/2001/XMLSchema#",
            "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
            "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
            "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
        }
        if is_credential(template):
            template = template
            template['credentialSubject']['@context'] = context
            template['credentialSubject']['gax-service:termsAndConditions']['gax-core:Value']['@value'] = "https://www.example.com/"
            template_signed = template
        else:
            template['@context'] = context
            template['gax-service:termsAndConditions']['gax-core:Value']['@value'] = "https://www.example.com/"
            template = {
                    "@context": [
                        "https://www.w3.org/2018/credentials/v1"
                    ],
                    "@id": f"{did}",
                    "issuer": f"{issuer}",
                    "@type": [
                        "VerifiableCredential",
                        "ServiceOffering"
                    ],
                    "credentialSubject": template
                }
            template_signed = None
        super().__init__(id, api, did, issuer, template, template_signed)
    
def is_credential(js):
    if js.get('credentialSubject'):
        return True
    return False

class SelfAssessedComplianceCriteriaClaim(AbstractCredential):
    def __init__(self, id, api, did, issuer, criterions, lso_did) -> None:
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "SelfAssessedComplianceCriteriaClaim"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@context": {
                        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#"
                    },
                    "gax-compliance:hasLocatedServiceOffering": lso_did,
                    "gax-compliance:hasAssessedComplianceCriteria": []
                }
            }
        for criterion in criterions:
            template.get('credentialSubject').get('gax-compliance:hasAssessedComplianceCriteria').append(
                {
                    "@type": "gax-compliance:ComplianceCriterion",
                    "@id": f"{criterion}"
                }
            )
        template_signed = None
        super().__init__(id, api, did, issuer, template, template_signed)

class SelfAssessedComplianceCriteriaCredential(AbstractCredential):
    def __init__(self, id, api, did, issuer, selfclaim) -> None:
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "SelfAssessedComplianceCriteriaCredential"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@type": "SelfAssessedComplianceCriteriaCredential",
                    "@context": {
                        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                        "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
                    },
                    "gax-compliance:credentialSubject": f"{selfclaim}",
                    "gax-compliance:isValid": {
                        "@value": "True"
                    }
                },
            }
        template_signed = None
        super().__init__(id, api, did, issuer, template, template_signed)
