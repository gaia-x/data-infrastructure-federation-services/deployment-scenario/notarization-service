VERSION = "v2206"

# Should be equal to these exposed by federated catalogue
CERTIFICATION_TYPES = ['C5 type I', 'C5 type II', 'CISPE Data Protection CoC', 
                        'CNDCP', 'CSA STAR Level 2', 'CSA STAR type I', 'CSA STAR type II', 
                        'Cyber Essentials Plus Certification', 'ENS', 'ENS  Espana (Royal Decree 3/2010)', 
                        'Fair Software Licensing', 'GSMA', 'HDS-FR', 'ISO 14001', 'ISO 20000', 'ISO 27001', 
                        'ISO 27017', 'ISO 27018', 'ISO 27701', 'ISO 45001', 'ISO 50001', 'ISO 9001', 'NEN 7510', 
                        'PCI-DSS', 'PCI_DSS_Level1', 'PiTuKri ISAE 3000 Type II', 'SAP : cloud and infrastructure', 
                        'SOC type I', 'SOC type II', 'SWIPO IaaS CoC', 'SecNumCloud', 'TISAX']

# API Form Fields
FORM_PARTICIPANT                = 'participant'
FORM_CERTIFICATION_TYPE         = 'certificationType'
FORM_SERVICE_OFFERING           = 'serviceOffering'
FORM_B64FILE                    = 'b64file'
FORM_LOCATED_SERVICE_OFFERING   = 'locatedServiceOffering'
FORM_QUESTIONNAIRE              = 'questionnaire'

# Internal Variables
ALLOWED_EXTENSIONS      = {'pdf', 'xml'}
ALLOWED_CLAIMS          = CERTIFICATION_TYPES

# Gaia-x Variables
DSS_NOTARIZATION_URL        = "https://notarization-service.abc-federation.gaia-x.community/services/rest/validation/validateSignature"
FEDERATION_VC_ISSUER_URL    = 'https://vc-issuer.abc-federation.gaia-x.community/api/v0.9/sign'
DUFOURSTORAGE_VC_ISSUER_URL = 'https://vc-issuer.dufourstorage.provider.gaia-x.community/api/v0.9/sign'
FEDERATED_CATALOGUE_URL     = "https://federated-catalogue-api.abc-federation.gaia-x.community/api"
COMPLIANCE_REFERENCES_URL   = f"{FEDERATED_CATALOGUE_URL}/compliance_references"
COMPLIANCE_CRITERIONS_URL   = "https://gaia-x.community/api/get_objects/compliance-criterion?canBeSelfAssessed=true"
FEDERATION_AGENT_URL        = "https://abc-federation.gaia-x.community/"
DUFOURSTORAGE_AGENT_URL     = "https://dufourstorage.provider.gaia-x.community/"

# Certif <-> Criterions
CERTIF_CRIT_LINK = {
    'ISO 27001': [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51],
    'SecNumCloud': [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51],
    'C5 type I': [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51],
    'C5 type II': [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51],
    'TISAX': [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51],
    'SOC type I': [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51],
    'SOC type II': [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51],
    'CSA STAR type I': [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51],
    'CSA STAR type II': [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51],
    'CISPE Data Protection CoC': [19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 54]
}

# Criterions
LOCAL_CRITERIONS = {
    1: 'did:web:gaia-x.community/compliance-criterion/cd0a48f986e723b784f8c8debcb3a43ebaf364db07f08d05ef52f9224df8165e/data.json',
    2: 'did:web:gaia-x.community/compliance-criterion/59af9d78fd07199558ec2c2cf69fbd4a1a25917b436cd6f8f8801d1041087ff2/data.json',
    3: 'did:web:gaia-x.community/compliance-criterion/16135616fc74f67304269a95f804ea09458d1b79ec8760dd053460bb92f21602/data.json',
    4: 'did:web:gaia-x.community/compliance-criterion/89a828ac39822e29e0e6f5eb12ee4c89066ff0356e0f07b9e06657ae0e7b7045/data.json',
    5: 'did:web:gaia-x.community/compliance-criterion/d645b42be387747fd299cc2efa7ccdd3ab507a0634d0339acde0f93f191a12f0/data.json',
    6: 'did:web:gaia-x.community/compliance-criterion/cb26bc2fca94ec1200495d126645838ef15405b7d09521f6c83626e9415aeda3/data.json',
    7: 'did:web:gaia-x.community/compliance-criterion/d7b1e613016f1b4c3cb784f9acced60dcdd03e90fcf0c15dd830b8594d685531/data.json',
    8: 'did:web:gaia-x.community/compliance-criterion/09a0d25b497d5ee6fab780171eb737af664e8daec98be8e6ef31d3650f977542/data.json',
    9: 'did:web:gaia-x.community/compliance-criterion/c91f5415d245fda056e8000fe285b6bb6a7e702d85964b1e89be45431146c35f/data.json',
    10: 'did:web:gaia-x.community/compliance-criterion/1ca1bf2e3da61db948dd4be88059a9ca0a7cbf8e142e6d8d3e02bfb0e9bbdb7d/data.json',
    11: 'did:web:gaia-x.community/compliance-criterion/95dff04c6b43304356c362ab152daccb6b172e1b264628b8f26eccb5ecf455c0/data.json',
    12: 'did:web:gaia-x.community/compliance-criterion/cfba3c7f822d223c68798ec62822f2f5371339125229ded80c2749d3f0c25f6a/data.json',
    13: 'did:web:gaia-x.community/compliance-criterion/3486ba011e4294f4dd13c280d149713797af53cb749ff8e92e31a5d7cfe3cc18/data.json',
    14: 'did:web:gaia-x.community/compliance-criterion/cbce011cbfb44b284f1667bcfbde29ae2fe03e8c84e4fe708fc7296200af9b32/data.json',
    15: 'did:web:gaia-x.community/compliance-criterion/8eb0f3637a25c29e76313c8a77bfbdda7697b7fa6a78789c17db481f8d0d2f47/data.json',
    16: 'did:web:gaia-x.community/compliance-criterion/1cfd158542516ce91cad8c6eff1829c2d3906a942f2fc6b9bb6868656997c4f2/data.json',
    17: 'did:web:gaia-x.community/compliance-criterion/53cd0eb3976ebc771cfd424ee7d4db6568e307d75168e2289525b63eefa79ca4/data.json',
    19: 'did:web:gaia-x.community/compliance-criterion/c22351d86e3fda98f200bacffc8e53b86583ab418d6308ab033944659647cf6e/data.json',
    20: 'did:web:gaia-x.community/compliance-criterion/caed57eaaa46833f0563f506b26759b9f128bd3be2aa0493f85f57d086f5b7f4/data.json',
    21: 'did:web:gaia-x.community/compliance-criterion/4542bf50aa210b8df3ac0942a4dd66f84584a8601bf458621edd322bcb9b93ec/data.json',
    22: 'did:web:gaia-x.community/compliance-criterion/1451e39bbfb14afc525cb9cabd8e96f9443b1720dc50fb99c292b382e386325b/data.json',
    23: 'did:web:gaia-x.community/compliance-criterion/cca79173862e03e244e3d5132dc42cfc5e3281085c6b910535b79d89c39a367f/data.json',
    24: 'did:web:gaia-x.community/compliance-criterion/753ef3b579ceb605c3635179ccba4b0a3119edc2965093af176dcb0fc8779bec/data.json',
    25: 'did:web:gaia-x.community/compliance-criterion/746e058bc4c6e4e263c671983ed267a322afb1290b105dbdfe0e2bbdf47986c0/data.json',
    26: 'did:web:gaia-x.community/compliance-criterion/95601c35870577b5d6f703acd92c46db9b24adeff01f120ef79ade116973606d/data.json',
    27: 'did:web:gaia-x.community/compliance-criterion/ebd24fa41e0bfca5bae38c6584416f0aac91fc7f8447b36e459f874dc17c237a/data.json',
    28: 'did:web:gaia-x.community/compliance-criterion/22aaad5f2713faae6bb0f7f62f27a28aa2ff7150d855aab4f96a1d1ba231f7eb/data.json',
    29: 'did:web:gaia-x.community/compliance-criterion/86cc86fea9ffee769fe1771721956e548c75a6f4e3789bc86f2007ddc065c718/data.json',
    30: 'did:web:gaia-x.community/compliance-criterion/69c68855d367bdb9a56410ccd480260c4f7c867985d0cf4871430616ec95a262/data.json',
    31: 'did:web:gaia-x.community/compliance-criterion/c127bb2a7ab00bdb260b1a275650ce4837d7ec49088306c713863d3bc2417025/data.json',
    32: 'did:web:gaia-x.community/compliance-criterion/75d2816960346bd79681fba13a295e8f70eae009da1020bd6d0a26d0a71179d1/data.json',
    33: 'did:web:gaia-x.community/compliance-criterion/21592f091e3885f982c456b436539f9d35145b4a9e0dea94cf628aa97c86d64e/data.json',
    34: 'did:web:gaia-x.community/compliance-criterion/f553932b59d3fc4dadcb61239cadab61540d42eb2b54a6d6c77f2ed3b3970f4c/data.json',
    35: 'did:web:gaia-x.community/compliance-criterion/bcf19550deea9190936f42d2f961afd375f19450a1f06580b27e59a0b87a2045/data.json',
    36: 'did:web:gaia-x.community/compliance-criterion/fdf5ebefcad6168f2796289d9985f0fb7dfceb9cd8cf2e65dd51e0314652c409/data.json',
    37: 'did:web:gaia-x.community/compliance-criterion/6f1374ddfee4e024dd8b467aeb08cf62bf3b524e27de4e1503aadc844076da98/data.json',
    38: 'did:web:gaia-x.community/compliance-criterion/3fffa2ee6d5283e5c00873155116617dfed86409f3ede93a66f3da732f6f22cb/data.json',
    39: 'did:web:gaia-x.community/compliance-criterion/9e7a05908004bf2e0a5e055450451cf410b8cceb8485ba5b85a0f07c7df6deaa/data.json',
    40: 'did:web:gaia-x.community/compliance-criterion/a00ad4418474407d648851e67d5691c104fbc63c2eb49e600f52a76443d10636/data.json',
    41: 'did:web:gaia-x.community/compliance-criterion/ed01ea98c844ce4568cb247db12fb348061d4b0651d90471a50395ca42720006/data.json',
    42: 'did:web:gaia-x.community/compliance-criterion/094812ba0119f254be6b3e64df03a147c645f6ad9080a1d997e9497e64fef896/data.json',
    43: 'did:web:gaia-x.community/compliance-criterion/a11fc3a2f615770787d3b7f53c1c3c06673981f13ddfabb3c5f04d8c4cdf1248/data.json',
    44: 'did:web:gaia-x.community/compliance-criterion/fcb4c1fd318cbd0fb7b4139064005a41fee56a523ff56843ede5ccb418642725/data.json',
    45: 'did:web:gaia-x.community/compliance-criterion/1b8f6bb84294a30f4f203bb065f9a51c2d3e979ed2f6f73b4b10e15d24894c55/data.json',
    46: 'did:web:gaia-x.community/compliance-criterion/e6cda69d8dfe71f828abb0ef9a1782948300ccaf73eefebe298cf5ce9956cc41/data.json',
    47: 'did:web:gaia-x.community/compliance-criterion/a4a187d0139d7ccd41dd47ade29ed2f07df13e4491d33b2855bd172277bb54ea/data.json',
    48: 'did:web:gaia-x.community/compliance-criterion/104d71a37aa04ea855e20390bcbeced8578fd69fdfe2f6790c1b1d2307a471a6/data.json',
    49: 'did:web:gaia-x.community/compliance-criterion/d687848eb2ce8246720e2e7bfcbe49adcf8355099669929c1d18e98ba6990b69/data.json',
    50: 'did:web:gaia-x.community/compliance-criterion/7e0d17541692ed994f527a3b80eb57fe56bdf5c10549b615a33e794bd36e0327/data.json',
    51: 'did:web:gaia-x.community/compliance-criterion/c708cf8add8753ff571867ba9335b645824e225507d3fe83797370bb59f4130d/data.json',
    52: 'did:web:gaia-x.community/compliance-criterion/a42cbd27cbad37fe56fa1c673c3e2e3a8b94a2a8a4d54e8cb97d7e3a1141ab3a/data.json',
    53: 'did:web:gaia-x.community/compliance-criterion/3fc472418cfe811bfaf55ef63fb9539ec3395d66ac661e50203faa1c2af3810d/data.json',
    54: 'did:web:gaia-x.community/compliance-criterion/1d1a117d37e43a7f9ed7970f301a3717359f730b89aaaa261460a4cd946ed4f3/data.json',
    55: 'did:web:gaia-x.community/compliance-criterion/3bfaea9a04fcb53c7878560a829553a8e1ef89b47e53ba8e5860668339c5d76e/data.json',
    56: 'did:web:gaia-x.community/compliance-criterion/7869d60a96f53d9c6234a3b975b299932f0331ae36d6e4bcd579e9bb2f1e25d1/data.json',
    57: 'did:web:gaia-x.community/compliance-criterion/9831fcfc20276e9530a022ec83d8f62d10707ccf1e774cbb1c6dfbc094216553/data.json',
    58: 'did:web:gaia-x.community/compliance-criterion/a4d50383aec3b321ab09ba0e52c1a882cf701c8f9c6ba4f3310d541e52fdf755/data.json',
    59: 'did:web:gaia-x.community/compliance-criterion/73f06ed0b87186de99380a5a8359f6803d329b86f462f8ad81bc21792206c270/data.json',
    60: 'did:web:gaia-x.community/compliance-criterion/b2b101611881ee41ae26d4b608c781337d5dc0230d619e9f1dde03c20288215a/data.json',
    61: 'did:web:gaia-x.community/compliance-criterion/f377b2b1a2071987253c5c099441426f980d0f8bd0042d4cdd2211ff8dd723fd/data.json'
}