import hashlib, string, random, json, os, requests, re
from .variables import *
from config import *

class FederationGenerator():
    def __init__(self, name) -> None:
        self.name = name
        
        self.id = hashlib.sha256(bytes(self.name, 'utf-8')).hexdigest()
        self.did = f"did:web:{self.name}.gaia-x.community"
        self.did_prefix = f"{self.did}:participant:{self.id}"
        
        self.vc_issuer_url = VC_ISSUER_URL.replace(NAME_TO_REPLACE, extract_info_from_did(self.did))
        self.agent_url = AGENT_URL.replace(NAME_TO_REPLACE, extract_info_from_did(self.did))
        self.store_agent_api = f"{self.agent_url}/api/store_object/"
        
        self.scheme_id = rand_id()
        self.scheme_type = 'third-party-compliance-certification-scheme'
        self.scheme_did = f"{self.did_prefix}/{self.scheme_type}/{self.scheme_id}/data.json"

        self.cab_id = self.id #TODO: see why ? rand_id()
        self.cab_type = "compliance-assessment-body"
        self.cab_did = f"{self.did_prefix}/{self.cab_type}/{self.cab_id}/data.json"

        self.claim_id  = rand_id()
        self.claim_type = 'third-party-compliance-certificate-claim'
        self.claim_did = f"{self.did_prefix}/{self.claim_type}/{self.claim_id}/data.json"

        self.cred_id = rand_id()
        self.cred_type = 'third-party-compliance-certificate-credential'
        self.cred_did = f"{self.did_prefix}/{self.cred_type}/{self.cred_id}/data.json"
    
    def get_name(self):
        return self.name
    def get_store_api(self):
        return self.store_agent_api
    def get_id(self):
        return self.id
    def get_did(self):
        return self.did
    def get_did_prefix(self):
        return self.did_prefix
    def get_vc_issuer_url(self):
        return self.vc_issuer_url
    def get_cab_did(self):
        return self.cab_did

class ParticipantGenerator():
    def __init__(self, name, role="provider") -> None:
        self.name = name
        
        self.id = hashlib.sha256(bytes(self.name, 'utf-8')).hexdigest()
        self.did = f"did:web:{self.name}.{role}.gaia-x.community"
        self.did_prefix = f"{self.did}:participant:{self.id}"
        
        self.vc_issuer_url = VC_ISSUER_URL.replace(NAME_TO_REPLACE, extract_info_from_did(self.did))
        self.agent_url = AGENT_URL.replace(NAME_TO_REPLACE, extract_info_from_did(self.did))
        self.store_agent_api = f"{self.agent_url}/api/store_object/"

        self.located_id = rand_id()
        self.located_type = 'located-service-offering'
        self.located_did = f"{self.did_prefix}/{self.located_type}/{self.located_id}/data.json"

        self.vc_id = rand_id()
        self.vc_type = 'vc'
        self.vc_did = f"{self.did_prefix}/{self.vc_type}/{self.vc_id}/data.json"

        self.vp_id = rand_id()
        self.vp_type = 'vp'
        self.vp_did = f"{self.did_prefix}/{self.vp_type}/{self.vp_id}/data.json"

        self.selfclaim_id = rand_id()
        self.selfclaim_type = 'self-assessed-compliance-criteria-claim'
        self.selfclaim_did = f"{self.did_prefix}/{self.selfclaim_type}/{self.selfclaim_id}/data.json"

        self.selfcred_id = rand_id()
        self.selfcred_type = 'self-assessed-compliance-criteria-credential'
        self.selfcred_did = f"{self.did_prefix}/{self.selfcred_type}/{self.selfcred_id}/data.json"

    def get_name(self):
        return self.name
    def get_store_api(self):
        return self.store_agent_api
    def get_id(self):
        return self.id
    def get_did(self):
        return self.did
    def get_did_prefix(self):
        return self.did_prefix
    def get_vc_issuer_url(self):
        return self.vc_issuer_url

# # # #
# Abstract Class for credential, composed with severals attributes :
#  - id : string (sha-256 of a random string)
#          "0106b44dae1c2cfe5e911a5c54eb10f434d38a4a64874447b7ecf4ce1b695d95"
#  - did : string (did) 
#           "did:web:dufourstorage.provider.gaia-x.community/participant/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/located-service-offering/0106b44dae1c2cfe5e911a5c54eb10f434d38a4a64874447b7ecf4ce1b695d95/data.json"
#  - obj_type : string (type of object)  
#           "located-service-offering"
#  - template : json (json object)
#  - template_signed : json (json object with signature alias proof)
# # # #
class AbstractCredential():
    def __init__(self, id, did, issuer, obj_type, template, template_signed) -> None:
        self.id = id
        self.did = did
        self.issuer = issuer
        
        self.obj_type = obj_type
        self.template = template
        self.template_signed = template_signed

        self.vc_issuer_url = f"https://vc-issuer.{extract_info_from_did(did)}.gaia-x.community/api/v0.9/sign"
        self.store_agent_api = f"https://{extract_info_from_did(did)}.gaia-x.community/api/store_object/"
    
    def get_id(self):
        return self.id
    
    def get_did(self):
        return self.did

    def get_template(self):
        return self.template

    def get_template_signed(self):
        if self.template_signed:
            return self.template_signed
        headers = {
                "Content-Type": "application/json; charset=UTF-8",
                "Accept": "application/json, application/javascript, text/javascript, text/json"
            }
        response = requests.put(self.vc_issuer_url, data=json.dumps(self.template), headers=headers)
        if response.status_code == 200:
            self.template_signed = json.loads(response.text)
            return self.template_signed
        else:
            print(f"Error HTTP Code {response.status_code} with {self.vc_issuer_url} for {self.obj_type} : {response.text}")
            return None

    def store_it(self):
        if LOCAL_ENV:
            self.store_to_file("./gen/")
        else:
            self.store_to_agent()

    def store_to_file(self, rac='.'):
        template_signed = self.get_template_signed()
        os.makedirs(f'{rac}/{self.obj_type}/{self.id}', exist_ok=True)
        with open(f'{rac}/{self.obj_type}/{self.id}/data.json', 'w') as f:
            json.dump(obj=template_signed, fp=f, indent=4)

    def store_to_agent(self):
        print(f'Store {self.obj_type} with id {self.did} on {self.store_agent_api}')
        headers = get_header_with_api_key()
        js = {'objectjson': json.dumps(self.get_template_signed())}
        response = requests.post(self.store_agent_api+self.obj_type, headers=headers, json=js)
        print(f'{response.status_code}: {response.text}')
        print(f'***')
        return response.text

class ThirdPartyComplianceCertificationScheme(AbstractCredential):
    def __init__(self, id, did, issuer, reference, criterions, cabs) -> None:
        obj_type = 'third-party-compliance-certification-scheme'
        
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "ThirdPartyComplianceCertificationScheme"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@type": "ThirdPartyComplianceCertificationScheme",
                    "@context": {
                        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                        "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
                    },
                    "gax-compliance:hasComplianceAssessmentBodies": [],
                    "gax-compliance:hasComplianceReference": {
                        "@type": "gax-compliance:ComplianceReference",
                        "@value": f"{reference}"
                    },
                    "gax-compliance:grantsComplianceCriteria": []
                }
            }
        for criterion in criterions:
            template.get('credentialSubject').get('gax-compliance:grantsComplianceCriteria').append(
                {
                    "@type": "gax-participant:ComplianceCriterion",
                    "@value": f"{criterion}"
                }
            )
        for cab in cabs:
            template.get('credentialSubject').get('gax-compliance:hasComplianceAssessmentBodies').append(
                {
                    "@type": "gax-participant:ComplianceAssessmentBody",
                    "@value": f"{cab}"
                }
            )
        template_signed = None
        super().__init__(id, did, issuer, obj_type, template, template_signed)
        
class ComplianceAssessmentBody(AbstractCredential):
    def __init__(self, id, did, issuer, scheme, claim, credential) -> None:
        obj_type = "compliance-assessment-body"
        
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "ComplianceAssessmentBody"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@type": "ComplianceAssessmentBody",
                    "@context": {
                        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                        "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
                    },
                    "gax-compliance:canCertifyThirdPartyComplianceCertificationScheme": [{
                            "@type": "gax-compliance:ThirdPartyComplianceCertificationScheme",
                            "@value": f"{scheme}"
                        }
                    ],
                    "gax-compliance:hasThirdPartyComplianceCertificateClaim": [{
                            "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim",
                            "@value": f"{claim}"
                        }
                    ],
                    "gax-compliance:hasThirdPartyComplianceCredential": [{
                            "@type": "gax-compliance:ThirdPartyComplianceCertificateCredential",
                            "@value": f"{credential}"
                        }
                    ]
                }
            }
        template_signed = None
        super().__init__(id, did, issuer, obj_type, template, template_signed)

class ThirdPartyComplianceCertificateClaim(AbstractCredential):
    def __init__(self, id, did, issuer, scheme, located, cab) -> None:
        obj_type = 'third-party-compliance-certificate-claim'
        
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "ThirdPartyComplianceCertificateClaim"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@type": "ThirdPartyComplianceCertificateClaim",
                    "@context": {
                        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                        "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
                        "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
                    },
                    "gax-compliance:hasComplianceCertificationScheme": {
                        "@type": "gax-compliance:ComplianceCertificationScheme",
                        "@value": f"{scheme}"
                    },
                    "gax-compliance:hasLocatedServiceOffering": {
                        "@type": "gax-service:LocatedServiceOffering",
                        "@value": f"{located}"
                    },
                    "gax-compliance:hasComplianceAssessmentBody": {
                        "@type": "gax-participant:ComplianceAssessmentBody",
                        "@value": f"{cab}"
                    }
                }
            }
        template_signed = None
        super().__init__(id, did, issuer, obj_type, template, template_signed)

class ThirdPartyComplianceCertificateCredential(AbstractCredential):
    def __init__(self, id, did, issuer, claim) -> None:
        obj_type = 'third-party-compliance-certificate-credential'
        
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "ThirdPartyComplianceCertificateCredential"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@type": "ThirdPartyComplianceCertificateCredential",
                    "@context": {
                        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                        "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
                    },
                    "@id": f"{claim}",
                    "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim",
                    "isValid": {
                        "@value": "True"
                    }
                },
            }
        template_signed = None
        super().__init__(id, did, issuer, obj_type, template, template_signed)

class LocatedServiceOffering(AbstractCredential): 
    def __init__(self, id, did, issuer, service_offering, locations, claims, creds) -> None:
        obj_type = 'located-service-offering'
        
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "LocatedServiceOffering"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@type": "LocatedServiceOffering",
                    "@context": {
                        "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
                    },
                    "gax-service:isImplementationOf": {
                        "@id": f"{service_offering}",
                        "@type": "gax-service:ServiceOffering"
                    },
                    "gax-service:isHostedOn": [],
                    "gax-service:hasComplianceCertificateClaim": [],
                    "gax-service:hasComplianceCertificateCredential": []
                }
            }
        for location in locations:
            template.get('credentialSubject').get('gax-service:isHostedOn').append(
                {
                    "@id": f"{location}",
                    "@type": "gax-participant:Location"
                }
            )
        for claim in claims:
            template.get('credentialSubject').get('gax-service:hasComplianceCertificateClaim').append(
                {
                    "@id": f"{claim}",
                    "@type": "gax-compliance:ComplianceCertificateClaim"
                }
            )
        for cred in creds:
            template.get('credentialSubject').get('gax-service:hasComplianceCertificateCredential').append(
                {
                    "@id": f"{cred}",
                    "@type": "gax-compliance:ComplianceCertificateCredential"
                }
            )
        template_signed = None
        super().__init__(id, did, issuer, obj_type, template, template_signed)

class LocatedServiceOfferingUpdate(AbstractCredential): 
    def __init__(self, template) -> None:
        id = template.get("@id").split('/')[-2]
        did = template.get("@id")
        issuer = template.get('issuer')
        obj_type = 'located-service-offering'
        template_signed = None
        super().__init__(id, did, issuer, obj_type, template, template_signed)
    
    def add_location(self, location):
        template = {
                "@id": f"{location}",
                "@type": "gax-participant:Location"
            }
        self.template.get('credentialSubject').get('gax-service:isHostedOn').append(template)

    def add_claim(self, claim, type):
        template = {
                "@id": f"{claim}",
                "@type": f"gax-compliance:{type}"
            }
        self.template.get('credentialSubject').get('gax-service:hasComplianceCertificateClaim').append(template)
    
    def add_credential(self, credential, type):
        template = {
                "@id": f"{credential}",
                "@type": f"gax-compliance:{type}"
            }
        self.template.get('credentialSubject').get('gax-service:hasComplianceCertificateCredential').append(template)
    
    def get_claims(self):
        return self.template.get('credentialSubject').get('gax-service:hasComplianceCertificateClaim')
    
    def get_credentials(self):
        return self.template.get('credentialSubject').get('gax-service:hasComplianceCertificateCredential')

class VerifiablePresentation(AbstractCredential):
    def __init__(self, id, did, issuer, vc_signeds) -> None:
        obj_type = 'vp'
        
        template = {
                "@context":[
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "@type": [
                    "VerifiablePresentation"
                ],
                "verifiableCredential": []
            }
        for vc_signed in vc_signeds:
            template.get('verifiableCredential').append(vc_signed)
        template_signed = None
        super().__init__(id, did, issuer, obj_type, template, template_signed)

class ServiceOffering(AbstractCredential): 
    def __init__(self, issuer, template) -> None:
        id = template.get("@id").split('/')[-2]
        did = template.get("@id")
        obj_type = 'service-offering'
        
        #context = {
        #    "dct": "http://purl.org/dc/terms/",
        #    "dcat": "http://www.w3.org/ns/dcat#",
        #    "xsd": "http://www.w3.org/2001/XMLSchema#",
        #    "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
        #    "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
        #    "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
        #}
        if is_credential(template):
            template = template
            #template['credentialSubject']['@context'] = context
            #template['credentialSubject']['gax-service:termsAndConditions']['gax-core:Value']['@value'] = "https://www.example.com/"
            template_signed = template
        else:
            #template['@context'] = context
            #template['gax-service:termsAndConditions']['gax-core:Value']['@value'] = "https://www.example.com/"
            template = {
                    "@context": [
                        "https://www.w3.org/2018/credentials/v1"
                    ],
                    "@id": f"{did}",
                    "issuer": f"{issuer}",
                    "@type": [
                        "VerifiableCredential",
                        "ServiceOffering"
                    ],
                    "credentialSubject": template
                }
            template_signed = None
        super().__init__(id, did, issuer, obj_type, template, template_signed)

class SelfAssessedComplianceCriteriaClaim(AbstractCredential):
    def __init__(self, id, did, issuer, criterions, lso_did) -> None:
        obj_type = 'self-assessed-compliance-criteria-claim'
        
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "SelfAssessedComplianceCriteriaClaim"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@context": {
                        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#"
                    },
                    "gax-compliance:hasLocatedServiceOffering": lso_did,
                    "gax-compliance:hasAssessedComplianceCriteria": []
                }
            }
        for criterion in criterions:
            template.get('credentialSubject').get('gax-compliance:hasAssessedComplianceCriteria').append(
                {
                    "@type": "gax-compliance:ComplianceCriterion",
                    "@id": f"{criterion}"
                }
            )
        template_signed = None
        super().__init__(id, did, issuer, obj_type, template, template_signed)

class SelfAssessedComplianceCriteriaCredential(AbstractCredential):
    def __init__(self, id, did, issuer, selfclaim) -> None:
        obj_type = 'self-assessed-compliance-criteria-credential'
        
        template = {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1"
                ],
                "@id": f"{did}",
                "issuer": f"{issuer}",
                "@type": [
                    "VerifiableCredential",
                    "SelfAssessedComplianceCriteriaCredential"
                ],
                "credentialSubject": {
                    "@id": f"{did}",
                    "@type": "SelfAssessedComplianceCriteriaCredential",
                    "@context": {
                        "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                        "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
                    },
                    "gax-compliance:credentialSubject": f"{selfclaim}",
                    "gax-compliance:isValid": {
                        "@value": "True"
                    }
                },
            }
        template_signed = None
        super().__init__(id, did, issuer, obj_type, template, template_signed)


def rand_id():
    return hashlib.sha256(bytes(''.join(random.choices(string.ascii_lowercase, k=10)), 'utf-8')).hexdigest()

def get_header_with_api_key():    
    #b64_key = API_KEY_AUTHORIZED
    #key = base64.standard_b64decode(b64_key).decode('UTF-8').replace('\n','')
    key = API_KEY_AUTHORIZED
    return {'x-api-key': key}

def is_credential(js):
    if js.get('credentialSubject'):
        return True
    return False

def extract_info_from_did(did):
    reg = "did:web:(.*?)\\.(provider|auditor)?"
    _, name, role, _ = re.split(reg, did)
    
    return f"{name}.{role}" if role else f"{name}"
    