VERSION = "v2210"

# Should be equal to these exposed by federated catalogue
CERTIFICATION_TYPES = ['C5 type I', 'C5 type II', 'CISPE Data Protection CoC', 
                        'CNDCP', 'CSA STAR Level 2', 'CSA STAR type I', 'CSA STAR type II', 
                        'Cyber Essentials Plus Certification', 'ENS', 'ENS  Espana (Royal Decree 3/2010)', 
                        'Fair Software Licensing', 'GSMA', 'HDS-FR', 'ISO 14001', 'ISO 20000', 'ISO 27001', 
                        'ISO 27017', 'ISO 27018', 'ISO 27701', 'ISO 45001', 'ISO 50001', 'ISO 9001', 'NEN 7510', 
                        'PCI-DSS', 'PCI_DSS_Level1', 'PiTuKri ISAE 3000 Type II', 'SAP : cloud and infrastructure', 
                        'SOC type I', 'SOC type II', 'SWIPO IaaS CoC', 'SecNumCloud', 'TISAX']

# API Form Fields
FORM_FEDERATION                 = 'federation'
FORM_PARTICIPANT                = 'participant'
FORM_CERTIFICATION_TYPE         = 'certificationType'
FORM_SERVICE_OFFERING           = 'serviceOffering'
FORM_B64FILE                    = 'b64file'
FORM_LOCATED_SERVICE_OFFERING   = 'locatedServiceOffering'
FORM_QUESTIONNAIRE              = 'questionnaire'

# Internal Variables
ALLOWED_EXTENSIONS      = {'pdf', 'xml'}
ALLOWED_CLAIMS          = CERTIFICATION_TYPES

# Variables used to patterns
NAME_TO_REPLACE             = "__NAME__"

# Gaia-x Variables
DSS_NOTARIZATION_URL        = "https://notarization-service.abc-federation.gaia-x.community/services/rest/validation/validateSignature"
VC_ISSUER_URL               = f"https://vc-issuer.{NAME_TO_REPLACE}.gaia-x.community/api/v0.9/sign"
AGENT_URL                   = f"https://{NAME_TO_REPLACE}.gaia-x.community"
FEDERATED_CATALOGUE_URL     = "https://federated-catalogue-api.abc-federation.gaia-x.community/api"
COMPLIANCE_REFERENCES_URL   = f"{FEDERATED_CATALOGUE_URL}/compliance_references"
COMPLIANCE_CRITERIONS_URL   = f"{FEDERATED_CATALOGUE_URL}/compliance_criterions?canBeSelfAssessed=true"

CRITERIONS         = None
REFERENCES         = None
REFERENCES_BY_NAME = None