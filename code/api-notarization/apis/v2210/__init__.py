import logging
from flask import Flask, Blueprint, render_template, request
from flask_cors import cross_origin
from .variables import *
from .ontology import *
from config import *

api = Blueprint('api_v2210', __name__)
logging.basicConfig(filename=LOG_FILE_NAME, level=logging.DEBUG)

# # # #
# GET /api/, return index page
# # # #
@api.route(rule='/', methods=['GET'])
@cross_origin()
def index_route():
    logging.info(f"{request.method} on /")
    return render_template(f'{VERSION}/index.html')

# # # #
# GET /api/status, return status of notarization service
#   no args
# # # #
@api.route(rule='/status', methods=['GET'])
@cross_origin()
def status_route():
    return format_response("success", "ok", 200)

# # # #
# GET /api/upload, return page for uploading pdf and generate VC
#   no args
# # # #
@api.route(rule='/upload_<nb>', methods=['GET'])
@cross_origin()
def upload_route(nb):
    logging.info(f"{request.method} on /upload_{nb}")
    return render_template(f'{VERSION}/upload_{nb}.html')
    
# # # #
# POST /api/create-vp, return VP of VC sent
#   args = json {
#           "federation": <string>,
#           "participant": <string>,
#           "locatedServiceOffering": <json>,
#           "questionnaire": <json>
#       }
# # # #
@api.route(rule='/create-vp', methods=['POST'])
@cross_origin()
def create_vp_route():
    logging.info(f"{request.method} on /create-vp")
    
    #Inputs check
    json_obj = request.get_json()

    #TODO: Add Federation in mandatory fields
    mandatory_fields = [FORM_PARTICIPANT, FORM_LOCATED_SERVICE_OFFERING, FORM_QUESTIONNAIRE]
    hasError = check_mandatory_fields_error(mandatory_fields, json_obj)
    if hasError:
        return format_response("error", hasError, 400)
    
    federation_name         = "abc-federation"
    participant_name        = json_obj.get(FORM_PARTICIPANT)
    locatedServiceOffering  = json_obj.get(FORM_LOCATED_SERVICE_OFFERING)
    questionnaire           = json_obj.get(FORM_QUESTIONNAIRE)
    
    participant_gen = ParticipantGenerator(participant_name)

    #Create n ComplianceCriterion
    criterions = [x.get('id') for x in questionnaire if x.get('value')]
    
    #Create SelfClaim //id, api, did, issuer, criterions
    selfclaim = SelfAssessedComplianceCriteriaClaim(participant_gen.selfclaim_id, participant_gen.selfclaim_did,
                    participant_gen.get_did(), criterions, locatedServiceOffering.get("@id"))

    #Create SelfCredential //id, api, did, issuer, selfclaim
    selfcredential = SelfAssessedComplianceCriteriaCredential(participant_gen.selfcred_id, participant_gen.selfcred_did,
                    participant_gen.get_did(), participant_gen.selfclaim_did)

    #Update locatedServiceOffering Object
    locatedSO = LocatedServiceOfferingUpdate(locatedServiceOffering)
    locatedSO.add_claim(selfclaim.get_did(), type="SelfAssessedComplianceCriteriaClaim")
    locatedSO.add_credential(selfcredential.get_did(), type="SelfAssessedComplianceCriteriaCredential")

    #Create VP
    vp = VerifiablePresentation(participant_gen.vp_id, participant_gen.vp_did,
                    participant_gen.get_did(), [locatedSO.get_template_signed()])
    vp_unsigned = vp.get_template()

    #Store all objects (to files or agent)
    #vp.store_it()
    selfclaim.store_it()
    selfcredential.store_it()
    locatedSO.store_it()

    return format_response("success", "VP unsigned generated", 200, None, vp_unsigned)

# # # #
# POST /api/create-vc, return VC if pdf is well signed
#   args = json {
#           "federation": <string>,
#           "participant": <string>,
#           "certificateType": <string>,
#           "b64file": <string>,
#           "serviceOffering": <json>,
#                    OR
#           "locatedServiceOffering": <json>
#       }
# # # #
@api.route(rule='/create-vc', methods=['POST'])
@cross_origin()
def create_vc_route():
    logging.info(f"{request.method} on /create-vc")
    
    #Inputs check
    json_obj = request.get_json()

    #TODO: Add Federation in mandatory fields
    mandatory_fields = [FORM_PARTICIPANT, FORM_CERTIFICATION_TYPE, FORM_B64FILE]
    
    #ServiceOffering or LocatedServiceOffering
    if FORM_SERVICE_OFFERING in json_obj:
        mandatory_fields.append(FORM_SERVICE_OFFERING)
    elif FORM_LOCATED_SERVICE_OFFERING in json_obj:
        mandatory_fields.append(FORM_LOCATED_SERVICE_OFFERING)
    else:
        return format_response("error", f"{FORM_SERVICE_OFFERING} or {FORM_LOCATED_SERVICE_OFFERING} is missing", 400)
    
    hasError = check_mandatory_fields_error(mandatory_fields, json_obj)
    if hasError:
        return format_response("error", hasError, 400)

    federation_name         = "abc-federation"
    participant_name        = json_obj.get(FORM_PARTICIPANT)
    certificationType       = json_obj.get(FORM_CERTIFICATION_TYPE)
    signed_pdf_b64          = json_obj.get(FORM_B64FILE).encode('UTF-8')
    serviceOffering         = json_obj.get(FORM_SERVICE_OFFERING)
    locatedServiceOffering  = json_obj.get(FORM_LOCATED_SERVICE_OFFERING)
    
    if certificationType not in get_all_references_by_name().keys():
        return format_response("error", f"Certification type ({certificationType}) not allowed", 400)
    
    #Check signature with DSS-Notarization
    isChecked, result, message, status = check_signature(b64encoded_doc_signed=signed_pdf_b64, 
                doc_signed_name="document")
    if not isChecked:
        return format_response(result, message, status)

    #Recreate federation and participant
    fede_gen = FederationGenerator(federation_name)
    participant_gen = ParticipantGenerator(participant_name)
    
    #Get ComplianceReference
    ref_did = get_all_references_by_name()[certificationType]
    ref_url = convert_did_to_https(ref_did)
    ack, ref = get_json_from_url(ref_url)
    if not ack:
        return format_response("error", ref, 400)
    
    #Get Schemes from ComplianceReference
    schemes = ref.get('gax-compliance:hasComplianceCertificationSchemes')
    scheme, scheme_did = None, None
    
    #Check if federation is included in cab of the scheme of the reference
    for s in schemes:
        scheme_tmp_did = s.get("@id")
        scheme_tmp_url = convert_did_to_https(scheme_tmp_did)
        ack, scheme_tmp = get_json_from_url(scheme_tmp_url)
        if not ack:
            return format_response("error", scheme_tmp, 400)
        cabs = scheme_tmp.get('gax-compliance:hasComplianceAssessmentBodies')
        for cab in cabs:
            cab_did = cab.get('@id')
            if federation_name in cab_did:
                scheme, scheme_did = scheme_tmp, scheme_tmp_did
                break
    #if federetion is not, federation is not allowed to issue claim of given certification
    if not scheme:
        return format_response("error", f"Error {federation_name} is not allowed to deliver {certificationType}", 400)
    
    #Create Claim
    claim = ThirdPartyComplianceCertificateClaim(fede_gen.claim_id, fede_gen.claim_did,
                    fede_gen.get_did(), scheme_did, participant_gen.located_did, fede_gen.cab_did)

    #Create Credential
    credential = ThirdPartyComplianceCertificateCredential(fede_gen.cred_id, fede_gen.cred_did,
                    fede_gen.get_did(), fede_gen.claim_did)
    
    #TODO: Need to update cab object of the federation
    
    #ServiceOffering or LocatedServiceOffering
    if serviceOffering:
        #Recreate ServiceOffering
        so = ServiceOffering(participant_gen.get_did(), serviceOffering)
        so.store_it()
        
        #Create LocatedServiceOffering
        service_offering_did = serviceOffering.get('@id')
        if is_credential(serviceOffering):
            location_did = serviceOffering.get('credentialSubject').get('gax-service:isAvailableOn')[0].get('@id')
        else:
            location_did = serviceOffering.get('gax-service:isAvailableOn')[0].get('@id')
        locatedSO = LocatedServiceOffering(participant_gen.located_id, participant_gen.located_did,
                        participant_gen.get_did(), service_offering_did, [location_did], [claim.get_did()], [credential.get_did()])
    elif locatedServiceOffering:
        #Update LocatedServiceOffering
        locatedSO = LocatedServiceOfferingUpdate(locatedServiceOffering)
        locatedSO.add_claim(claim.get_did(), type="ThirdPartyComplianceCertificateClaim")
        locatedSO.add_credential(credential.get_did(), type="ThirdPartyComplianceCertificateCredential")
    else:
        format_response("error", f"{FORM_SERVICE_OFFERING} or {FORM_LOCATED_SERVICE_OFFERING} is missing", 400)
    
    claim.store_it()
    credential.store_it()
    locatedSO.store_it()
    
    vc_signed = locatedSO.get_template_signed()

    return format_response(result, message, status, vc_signed, None)


# # # #
# FCT, verify signature
# # # #
def check_signature(b64encoded_doc_signed, doc_signed_name, b64encoded_doc_origin=None, doc_origin_name=None):
    return True, "success", "Valid signature", 200

    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Accept": "application/json, application/javascript, text/javascript, text/json"
    }
    data = {
        "signedDocument" : {
            "bytes" : b64encoded_doc_signed.decode('UTF-8'),
            "digestAlgorithm" : None,
            "name" : doc_signed_name
        },
        "policy" : None,
        "tokenExtractionStrategy" : "NONE",
        "signatureId" : None
    }
    if b64encoded_doc_origin:
        data["originalDocuments"] = [ {
            "bytes" : b64encoded_doc_origin.decode('UTF-8'),
            "digestAlgorithm" : None,
            "name" : doc_origin_name
        } ]
    
    response = requests.post(DSS_NOTARIZATION_URL, data=json.dumps(data), headers=headers)

    if response.status_code == 200:
        response = json.loads(response.text)
        simpleReport = response["SimpleReport"]
        
        if simpleReport['ValidSignaturesCount'] > 0:
            return True, "success", "Valid signature", 200
        else:
            return False, "error", "No valid signature fund", 400
    else:
        return False, "error", f"Error HTTP Code {response.status_code} with {DSS_NOTARIZATION_URL} : {response.text}", 400

# # # #
# FCT, verify if file extension is allowed
# # # #
def allowed_file_extension(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# # # #
# FCT, format response to service rest
# # # #
def format_response(result, message, status, vc="", vp=""):
    if result == "error":
        logging.error(message)
    elif result == "success":
        logging.info(message)
    
    return Flask.response_class(
        response=json.dumps({"result": result, "message": message, "vc": vc, "vp": vp}),
        status=status,
        mimetype="aplication/json"
    )

def check_mandatory_fields_error(mandatory_fields, json_obj):
    for field in mandatory_fields:
        if field not in json_obj:
            return f"No {field}"
        else:
            val = json_obj.get(field)
            if val in [None, '']:
                return f"{field} cannot be null"
    return None

def get_all_criterions():
    try:
        if CRITERIONS:
            return CRITERIONS
    except:
        pass
            
    criterions = {}
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Accept": "application/json, application/javascript, text/javascript, text/json"
    }
    ack, crits = get_json_from_url(url=COMPLIANCE_CRITERIONS_URL, data=None, headers=headers)
    
    if ack:
        for crit in crits:
            did = crit.get('@id')
            criterions[did] = crit
    else:
        logging.error(crits)
    
    CRITERIONS = criterions
    
    return CRITERIONS

def get_all_criterions_by_name():
    try:
        if CRITERIONS:
            return CRITERIONS
    except:
        pass
            
    criterions = {}
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Accept": "application/json, application/javascript, text/javascript, text/json"
    }
    ack, crits = get_json_from_url(url=COMPLIANCE_CRITERIONS_URL, data=None, headers=headers)
    
    if ack:
        for crit in crits:
            name = crit.get('gax-compliance:hasName').get('@value')
            criterions[name] = crit
    else:
        logging.error(crits)
    
    CRITERIONS = criterions

    return CRITERIONS

def get_all_references():
    try:
        if REFERENCES:
            return REFERENCES
    except:
        pass
        
    references = {}
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Accept": "application/json, application/javascript, text/javascript, text/json"
    }
    ack, res = get_json_from_url(url=COMPLIANCE_REFERENCES_URL, data=None, headers=headers)
    
    if ack:
        for r in res:
            did = r.get('compliance_ref_did')
            references[did] = r
    else:
        logging.error(res)
    
    REFERENCES = references
    
    return REFERENCES

def get_all_references_by_name():
    try:
        if REFERENCES_BY_NAME:
            return REFERENCES_BY_NAME

    except:
        pass
        
    references_by_name = {}
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Accept": "application/json, application/javascript, text/javascript, text/json"
    }
    ack, res = get_json_from_url(url=COMPLIANCE_REFERENCES_URL, data=None, headers=headers)
    
    if ack:
        for r in res:
            did = r.get('compliance_ref_did')
            title = r.get('title')
            version = r.get('version')
            if version:
                references_by_name[f"{title} {version}"] = did
            else:
                references_by_name[f"{title}"] = did
    else:
        logging.error(res)
    
    REFERENCES_BY_NAME = references_by_name
    
    return REFERENCES_BY_NAME

def convert_did_to_https(did):
    return f"https://{did.replace('did:web:', '').replace(':', '/')}"

def get_json_from_url(url, data=None, headers={}):
    js = None
    try:
        response = requests.get(url=url, data=data, headers=headers)
        if response.status_code == 200:
            js = response.json()
        else:
            return False, f"Error HTTP Code {response.status_code} with {url} : {response.text}"
    except Exception as err:
        return False, f"Error with {url}: {err}, {type(err)}"
    
    return True, js
