import logging
from flask import Flask, Blueprint, render_template, request
from flask_cors import cross_origin
from .variables import *
from .ontology import *
from config import *


api = Blueprint(f'api_{VERSION}', __name__)
logging.basicConfig(filename=LOG_FILE_NAME, level=logging.DEBUG)


@api.route(rule='/', methods=['GET'])
@cross_origin()
def index_route():
    """
    Returns index page
    
    Parameters:
       n/a
    
    Returns:
       index html page
    """
    logging.info(f"{request.method} on /")
    return render_template(f'{VERSION}/index.html')


@api.route(rule='/status', methods=['GET'])
@cross_origin()
def status_route():
    """
    Returns the status of notarization service
    
    Parameters:
       n/a
    
    Returns:
       json response with status info of notarization service
    """
    return format_response("success", "ok", 200)


@api.route(rule='/create-compliance-certificate-credential', methods=['POST'])
@cross_origin()
def create_compliance_certificate_credential_route():
    """
    Returns json object with credential requested after signature verification of the pdf given in argument
    
    Parameters:
        json {
           "federation": <string>,
           "participant": <string>,
           "certification": <string>,
           "certificateCredential": <string>,
           "b64file": <string>,
           "serviceOffering": <json> or <string>
       }
    
    Returns:
        json {
            result = <string> ("success" or "error"),
            message = <string>,
            status = <int> (http status code), 
            vc_unsigned = <json>,
            vc = <json>
        }
    """
    logging.info(f"{request.method} on /create-compliance-certificate-credential")
    #compliance-certificate-credential
    
    #Inputs check
    json_obj = request.get_json()

    #TODO: Add Federation in mandatory fields
    mandatory_fields = [FORM_FEDERATION, FORM_PARTICIPANT, FORM_CERTIFICATION_TYPE, FORM_B64FILE, FORM_SERVICE_OFFERING]
    
    hasError = check_mandatory_fields_error(mandatory_fields, json_obj)
    if hasError:
        return format_response(result = "error", message = hasError, status = 400)

    federation_name         = json_obj.get(FORM_FEDERATION, "abc-federation")
    participant_name        = json_obj.get(FORM_PARTICIPANT)
    certification           = json_obj.get(FORM_CERTIFICATION_TYPE)
    signed_pdf_b64          = json_obj.get(FORM_B64FILE).encode('UTF-8')
    service_offering        = json_obj.get(FORM_SERVICE_OFFERING)
    
    #service_offering is object or id ?
    if not isinstance(service_offering, dict):
        so_did = service_offering
        # Test if ServiceOffering exists
        ack, service_offering = get_json_from_uri(service_offering)
        if not ack:
            return format_response("error", service_offering, 400)
    so_did = service_offering.get("@id", service_offering.get("id"))
    
    if get_all_reference_ids() == 0:
        return format_response("error", f"Cannot get certification references...", 400)
    if certification not in get_all_reference_ids():
        return format_response("error", f"Certification does not exist", 400)
    
    #Check signature with DSS-Notarization
    isChecked, result, message, status = check_signature(
        b64encoded_doc_signed = signed_pdf_b64, 
        doc_signed_name = "document")
    if not isChecked:
        return format_response(result=result, message=message, status=status)
    
    ack, ref = get_json_from_uri(certification)
    if not ack:
        return format_response("error", ref, 400)
    
    #Get Schemes from ComplianceReference
    schemes = ref.get('gax-compliance:hasComplianceCertificationSchemes', ref.get('gx-compliance:hasComplianceCertificationSchemes'))
    scheme_did = None
    
    #Get valid scheme (type is ComplianceCertificationScheme)
    for s in schemes:
        scheme_tmp_type = s.get("@type", s.get("type"))
        if scheme_tmp_type in ["gax-compliance:ComplianceCertificationScheme", "gx-compliance:ComplianceCertificationScheme"]:
            scheme_did = s.get("@id", s.get("id"))
            break

    if not scheme_did:
        return format_response("error", f"Error {participant_name} is not allowed to self-declared given certification", 400)
    
    participant = SingletonFactory.get_instance(name=participant_name, role="provider")
    
    #Create Claim
    cc_claim = ComplianceCertificateClaim(
        issuer = participant, 
        scheme_did = scheme_did,
        so_did = so_did
    )

    #Create Credential
    cc_credential = ComplianceCertificateCredential(
        issuer = participant, 
        claim_did = cc_claim.get_did()
    )

    return format_response(
        result = result, 
        message = message, 
        status = status, 
        vc_unsigned = cc_credential.get_vc_unsigned(),
        vc = cc_credential.get_vc_signed())


@api.route(rule='/create-notary-compliance-certificate-credential', methods=['POST'])
@cross_origin()
def create_notary_compliance_certificate_credential_route():
    """
    Returns json object with credential requested (same as input in this specifc case of third party case)
    
    Parameters:
        json {
           "federation": <string>,
           "participant": <string>,
           "certificateType": <string>,
           "b64file": <string>,
           "serviceOffering": <json> or <string>
       }
       
    Returns:
        json {
            result = <string> ()"success" or "error"),
            message = <string>,
            status = <int> (http status code), 
            vc_unsigned = <json>,
            vc = <json>
        }
    """
    logging.info(f"{request.method} on /create-third-party-compliance-certificate-credential")
    #third-party-compliance-certificate-credential
    
    #Inputs check
    json_obj = request.get_json()

    #TODO: Add Federation in mandatory fields
    mandatory_fields = [FORM_PARTICIPANT, FORM_CERTIFICATION_TYPE, FORM_CERTIFICATION_TYPE, FORM_SERVICE_OFFERING]
    
    hasError = check_mandatory_fields_error(mandatory_fields, json_obj)
    if hasError:
        return format_response(
            result = "error", 
            message = hasError,
            status = 400)

    #federation_name_tmp     = json_obj.get(FORM_FEDERATION)
    federation_name             = "abc-federation"
    participant_name            = json_obj.get(FORM_PARTICIPANT)
    certification_type          = json_obj.get(FORM_CERTIFICATION_TYPE)
    certification_credential    = json_obj.get(FORM_CERTIFICATION_CREDENTIAL)
    service_offering            = json_obj.get(FORM_SERVICE_OFFERING)
    so_did                      = None
    
    #service_offering is object or id ?
    if isinstance(service_offering, dict):
        so_did = service_offering.get("@id", service_offering.get("id"))
    else:
        so_did = service_offering
        # Test if ServiceOffering exists
        ack, so = get_json_from_uri(so_did)
        if not ack:
            return format_response("error", so, 400)
        
    if not isinstance(certification_credential, dict):
        return format_response("error", f"Certification credential is not valid", 400)
    
    if certification_type not in get_all_references_by_name().keys():
        return format_response("error", f"Certification type ({certification_type}) not allowed", 400)
    
    #Verification is issuer of credential is allowed to deliver credential for certification
    #Get ComplianceReference
    if get_all_references_by_name() == 0:
        return format_response("error", f"Cannot get certification references...", 400)
    ref_did = get_all_references_by_name()[certification_type]
    ref_url = convert_did_to_https(ref_did)
    ack, ref = get_json_from_uri(ref_url)
    if not ack:
        return format_response("error", ref, 400)
    
    #Get Schemes from ComplianceReference
    schemes = ref.get('gax-compliance:hasComplianceCertificationSchemes', ref.get('gx-compliance:hasComplianceCertificationSchemes'))
    scheme, scheme_did = None, None
    
    issuer_did = certification_credential.get('issuer')
    #Check if issuer of credential is included in cab of the scheme of the reference
    for s in schemes:
        scheme_tmp_did = s.get("@id", s.get("id"))
        scheme_tmp_url = convert_did_to_https(scheme_tmp_did)
        ack, scheme_tmp = get_json_from_uri(scheme_tmp_url)
        if not ack:
            return format_response("error", scheme_tmp, 400)
        cabs = scheme_tmp.get('gax-compliance:hasComplianceAssessmentBodies', scheme_tmp.get('gx-compliance:hasComplianceAssessmentBodies'))
        for cab in cabs:
            cab_did = cab.get('@id', cab.get('id'))
            if issuer_did in cab_did:
                scheme, scheme_did = scheme_tmp, scheme_tmp_did
                break
    #if federetion is not, federation is not allowed to issue claim of given certification
    if not scheme:
        return format_response("error", f"Error {issuer_did} is not allowed to deliver {certification_type}", 400)

    return format_response(
        result = "success", 
        message = "Certification Credential is valid", 
        status = 200, 
        vc_unsigned = None,
        vc = certification_credential)


@api.route(rule='/create-third-party-compliance-certificate-credential', methods=['POST'])
@cross_origin()
def create_third_party_compliance_certificate_credential_route():
    """
    Returns json object with credential requested (same as input in this specifc case of third party case)
    
    Parameters:
        json {
           "federation": <string>,
           "participant": <string>,
           "certificateType": <string>,
           "certificationCredential": <json> or <string>,
           "serviceOffering": <json> or <string>
       }
       
    Returns:
        json {
            result = <string> ()"success" or "error"),
            message = <string>,
            status = <int> (http status code), 
            vc_unsigned = <json>,
            vc = <json>
        }
    """
    logging.info(f"{request.method} on /create-third-party-compliance-certificate-credential")
    #third-party-compliance-certificate-credential
    
    #Inputs check
    json_obj = request.get_json()

    #TODO: Add Federation in mandatory fields
    mandatory_fields = [FORM_PARTICIPANT, FORM_CERTIFICATION_TYPE, FORM_CERTIFICATION_TYPE, FORM_SERVICE_OFFERING]
    
    hasError = check_mandatory_fields_error(mandatory_fields, json_obj)
    if hasError:
        return format_response(
            result = "error", 
            message = hasError,
            status = 400)

    #federation_name_tmp     = json_obj.get(FORM_FEDERATION)
    federation_name             = "abc-federation"
    participant_name            = json_obj.get(FORM_PARTICIPANT)
    certification_type          = json_obj.get(FORM_CERTIFICATION_TYPE)
    certification_credential    = json_obj.get(FORM_CERTIFICATION_CREDENTIAL)
    service_offering            = json_obj.get(FORM_SERVICE_OFFERING)
    so_did                      = None
    
    #service_offering is object or id ?
    if isinstance(service_offering, dict):
        so_did = service_offering.get("@id", service_offering.get("id"))
    else:
        so_did = service_offering
        # Test if ServiceOffering exists
        ack, so = get_json_from_uri(convert_did_to_https(so_did))
        if not ack:
            return format_response("error", so, 400)
        
    if not isinstance(certification_credential, dict):
        return format_response("error", f"Certification credential is not valid", 400)
    
    if certification_type not in get_all_references_by_name().keys():
        return format_response("error", f"Certification type ({certification_type}) not allowed", 400)
    
    #Verification is issuer of credential is allowed to deliver credential for certification
    #Get ComplianceReference
    if get_all_references_by_name() == 0:
        return format_response("error", f"Cannot get certification references...", 400)
    ref_did = get_all_references_by_name()[certification_type]
    ref_url = convert_did_to_https(ref_did)
    ack, ref = get_json_from_uri(ref_url)
    if not ack:
        return format_response("error", ref, 400)
    
    #Get Schemes from ComplianceReference
    schemes = ref.get('gax-compliance:hasComplianceCertificationSchemes', ref.get('gx-compliance:hasComplianceCertificationSchemes'))
    scheme, scheme_did = None, None
    
    issuer_did = certification_credential.get('issuer')
    #Check if issuer of credential is included in cab of the scheme of the reference
    for s in schemes:
        scheme_tmp_did = s.get("@id", s.get("id"))
        scheme_tmp_url = convert_did_to_https(scheme_tmp_did)
        ack, scheme_tmp = get_json_from_uri(scheme_tmp_url)
        if not ack:
            return format_response("error", scheme_tmp, 400)
        cabs = scheme_tmp.get('gax-compliance:hasComplianceAssessmentBodies', scheme_tmp.get('gx-compliance:hasComplianceAssessmentBodies'))
        for cab in cabs:
            cab_did = cab.get('@id', cab.get('id'))
            if issuer_did in cab_did:
                scheme, scheme_did = scheme_tmp, scheme_tmp_did
                break
    #if federetion is not, federation is not allowed to issue claim of given certification
    if not scheme:
        return format_response("error", f"Error {issuer_did} is not allowed to deliver {certification_type}", 400)

    return format_response(
        result = "success", 
        message = "Certification Credential is valid", 
        status = 200, 
        vc_unsigned = None,
        vc = certification_credential)


@api.route(rule='/create-self-assessed-compliance-criteria-credential', methods=['POST'])
@cross_origin()
def create_self_assessed_compliance_criteria_credential_route():
    """
    Returns json object with requested self assessed compliance criteria credential related to questionnaire send in parameters
    
    Parameters:
        json {
           "federation": <string>,
           "participant": <string>,
           "questionnaire": <json>,
           "serviceOffering": <string> or <json>
       }
       
    Returns:
        json {
            result = <string> ("success" or "error"),
            message = <string>,
            status = <int> (http status code), 
            vc_unsigned = <json>,
            vc = <json>
        }
    """
    logging.info(f"{request.method} on /create-self-assessed-compliance-criteria-credential")
    
    #Inputs check
    json_obj = request.get_json()

    #TODO: Add Federation in mandatory fields
    mandatory_fields = [FORM_PARTICIPANT, FORM_SERVICE_OFFERING, FORM_QUESTIONNAIRE]
    hasError = check_mandatory_fields_error(mandatory_fields, json_obj)
    if hasError:
        return format_response(
            result = "error", 
            message = hasError, 
            status = 400)
    
    federation_name         = "abc-federation"
    participant_name        = json_obj.get(FORM_PARTICIPANT)
    service_offering        = json_obj.get(FORM_SERVICE_OFFERING)
    questionnaire           = json_obj.get(FORM_QUESTIONNAIRE)
    so_did                  = None
    
    if isinstance(service_offering, dict):
        so_did = service_offering.get("@id")
    else:
        so_did = service_offering
        # Test if ServiceOffering exists
        ack, so = get_json_from_uri(convert_did_to_https(so_did))
        if not ack:
            return format_response("error", so, 400)
    
    participant_gen = ParticipantGenerator(participant_name)

    #Create n ComplianceCriterion
    criterions = [x.get('id') for x in questionnaire if x.get('value')]
    
    #Create Self-Claim
    claim = SelfAssessedComplianceCriteriaClaim(
        id = participant_gen.selfclaim_id, 
        did = participant_gen.selfclaim_did,
        issuer = participant_gen.get_did(), 
        criterions = criterions, 
        so_did = so_did)

    #Create Self-Credential
    credential = SelfAssessedComplianceCriteriaCredential(
        id = participant_gen.selfcred_id, 
        did = participant_gen.selfcred_did,
        issuer = participant_gen.get_did(), 
        selfclaim = participant_gen.selfclaim_did
    )

    #Store all objects (to files or agent)
    claim.store_it()
    credential.store_it()

    return format_response(
        result = "success", 
        message = "VP unsigned generated", 
        status = 200, 
        vc_unsigned = credential.get_template(),
        vc = credential.get_template_signed())


def check_signature(b64encoded_doc_signed, doc_signed_name, b64encoded_doc_origin=None, doc_origin_name=None):
    """
    Returns signature verification of given base64encoded file
    
    Parameters:
        b64encoded_doc_signed: <string>
        doc_signed_name: <string>
        b64encoded_doc_origin: <string>
        doc_origin_name: <string>
       
    Returns: array composed of 
        result: <boolean> 
        message: <string> ()"error" or "success")
        status: <int> (http status code)
    """
    return True, "success", "Valid signature", 200

    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Accept": "application/json, application/javascript, text/javascript, text/json"
    }
    data = {
        "signedDocument" : {
            "bytes" : b64encoded_doc_signed.decode('UTF-8'),
            "digestAlgorithm" : None,
            "name" : doc_signed_name
        },
        "policy" : None,
        "tokenExtractionStrategy" : "NONE",
        "signatureId" : None
    }
    if b64encoded_doc_origin:
        data["originalDocuments"] = [ {
            "bytes" : b64encoded_doc_origin.decode('UTF-8'),
            "digestAlgorithm" : None,
            "name" : doc_origin_name
        } ]
    
    response = requests.post(DSS_NOTARIZATION_URL, data=json.dumps(data), headers=headers)

    if response.status_code == 200:
        response = json.loads(response.text)
        simpleReport = response["SimpleReport"]
        
        if simpleReport['ValidSignaturesCount'] > 0:
            return True, "success", "Valid signature", 200
        else:
            return False, "error", "No valid signature fund", 400
    else:
        return False, "error", f"Error HTTP Code {response.status_code} with {DSS_NOTARIZATION_URL} : {response.text}", 400


def allowed_file_extension(filename):
    """
    Returns if file extension is allowed
    
    Parameters:
        filename: <string>
       
    Returns:
        result: <boolean> 
    """
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def format_response(result, message, status, vc_unsigned=None, vc=None, vp_unsigned=None, vp=None):
    """
    Returns response format with concatenation of data into json object
    
    Parameters:
        result: <json>,
        message: <string>
        status: <int>
        vc_unsigned: <json>
        vc: <json>
        vp_unsigned: <json>
        vp: <json>
        
    Returns:
        <json>
    """
    if result == "error":
        logging.error(message)
    elif result == "success":
        logging.info(message)
    
    return Flask.response_class(
        response=json.dumps({"result": result, "message": message, "vc_unsigned": vc_unsigned, "vc": vc, "vp_unsigned": vp_unsigned, "vp": vp}),
        status=status,
        mimetype="aplication/json"
    )


def check_mandatory_fields_error(mandatory_fields, json_obj):
    for field in mandatory_fields:
        if field not in json_obj:
            return f"No {field}"
        else:
            val = json_obj.get(field)
            if val in [None, '']:
                return f"{field} cannot be null"
    return None


def get_all_criterion_ids():
    try:
        if CRITERION_IDS:
            return CRITERION_IDS
    except:
        pass
            
    criterions = []
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Accept": "application/json, application/javascript, text/javascript, text/json"
    }
    ack, crits = get_json_from_uri(uri=COMPLIANCE_CRITERIONS_URL, data=None, headers=headers)
    
    if ack:
        for crit in crits:
            did = crit.get('@id', crit.get('id'))
            criterions.append(did)
    else:
        logging.error(crits)
    
    CRITERION_IDS = criterions
    
    return CRITERION_IDS


def get_all_reference_ids():
    try:
        if REFERENCE_IDS:
            return REFERENCE_IDS
    except:
        pass
        
    references = []
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
        "Accept": "application/json, application/javascript, text/javascript, text/json"
    }
    ack, res = get_json_from_uri(uri=COMPLIANCE_REFERENCES_URL, data=None, headers=headers)
    
    if ack:
        for r in res:
            did = r.get('compliance_ref_did')
            references.append(did)
    else:
        logging.error(res)
    
    REFERENCE_IDS = references
    
    return REFERENCE_IDS


def convert_did_to_https(did):
    return f"https://{did.replace('did:web:', '').replace(':', '/')}"


def get_json_from_uri(uri, data=None, headers={}):
    js = None
    if uri.startswith('did:web'):
        url = convert_did_to_https(uri)
    else:
        url = uri
    try:
        response = requests.get(url=url, data=data, headers=headers)
        if response.status_code == 200:
            js = response.json()
        else:
            return False, f"Error HTTP Code {response.status_code} with {url} : {response.text}"
    except Exception as err:
        return False, f"Error with {url}: {err}, {type(err)}"
    
    return True, js
