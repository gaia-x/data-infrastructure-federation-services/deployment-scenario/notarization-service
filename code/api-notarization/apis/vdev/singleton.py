import re, hashlib

class SingletonFactory:
    _instances = {}

    @staticmethod
    def get_instance(name, role=None):
        key = (name, role)
        instance = SingletonFactory._instances.get(key)

        if instance is None:
            instance = SingletonFactory._create_instance(name, role)
            SingletonFactory._instances[key] = instance

        return instance

    @staticmethod
    def _create_instance(name, role):
        instance = Participant(name, role)
        return instance

class Participant():
    def __init__(self, name, role="provider") -> None:
        self.name = name
        
        self.id = hashlib.sha256(bytes(self.name, 'utf-8')).hexdigest()
        self.did = f"did:web:{self.name}.{role}.gaia-x.community"
        
        self.sign_api = f"https://vc-issuer.{extract_info_from_did(self.did)}.gaia-x.community/api/v0.9/sign"
        self.store_api = f"https://{extract_info_from_did(self.did)}.gaia-x.community/api/store_object/"

    def get_name(self):
        return self.name
    def get_id(self):
        return self.id
    def get_did(self):
        return self.did
    def get_sign_api(self):
        return self.sign_api
    def get_store_api(self):
        return self.store_api

def extract_info_from_did(did):
    reg = "did:web:(.*?)\.(provider|auditor)?"
    _, name, role, _ = re.split(reg, did)
    
    return f"{name}.{role}" if role else f"{name}"

if __name__ == "__main__":
    # The client code.

    federation1 = SingletonFactory.get_instance(name="abc-federation")
    federation2 = SingletonFactory.get_instance(name="abc-federation")
    dufourstorage1 = SingletonFactory.get_instance(name="dufourstorage", role="provider")
    dufourstorage2 = SingletonFactory.get_instance(name="dufourstorage", role="provider")
    
    print(f"federation1 is federation2: {federation1 is federation2}")
    print(f"dufourstorage1 is dufourstorage2: {dufourstorage1 is dufourstorage2}")
    print(f"federation1 is dufourstorage1: {federation1 is dufourstorage1}")