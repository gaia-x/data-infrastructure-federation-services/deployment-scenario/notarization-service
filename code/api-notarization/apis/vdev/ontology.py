import hashlib, string, random, json, os, requests, re
from typing import Any
from .variables import *
from config import *


class SingletonFactory:
    _instances = {}

    @staticmethod
    def get_instance(name: str, role=None):
        key = (name, role)
        instance = SingletonFactory._instances.get(key)

        if instance is None:
            instance = SingletonFactory._create_instance(name, role)
            SingletonFactory._instances[key] = instance

        return instance

    @staticmethod
    def _create_instance(name: str, role: str):
        instance = Participant(name, role)
        return instance


class Participant():
    def __init__(self, name: str, role=None) -> None:
        self.name = name
        
        self.id = hashlib.sha256(bytes(name, 'utf-8')).hexdigest()
        self.did = f"did:web:{name}.{role}.gaia-x.community"
        
        self.role = role
        
        self.sign_api = f"https://vc-issuer.{extract_info_from_did(self.did)}.gaia-x.community/api/v0.9/sign"
        self.store_api = f"https://{extract_info_from_did(self.did)}.gaia-x.community/api/store_object/"

    def get_name(self):
        return self.name
    def get_id(self):
        return self.id
    def get_did(self):
        return self.did
    def get_role(self):
        return self.role
    def get_info(self):
        return f"{self.name}.{self.role}" if self.role else f"{self.name}"
    def get_sign_api(self):
        return self.sign_api
    def get_store_api(self):
        return self.store_api


class AbstractVerifiableCredential():
    def __init__(self, issuer: Participant, cred_dict: dict) -> None:
        self.issuer = issuer
        
        self.vc_type = 'VerifiableCredential'
        self.vc_agent_type = 'vc'
        self.vc_id = rand_id()
        self.vc_did = f"{issuer.get_did()}:participant:{issuer.get_id()}/{self.vc_agent_type}/{self.vc_id}/data.json"

        self.sign_api = f"https://vc-issuer.{issuer.get_info()}.gaia-x.community/api/v0.9/sign"
        self.store_api = f"https://{issuer.get_info()}.gaia-x.community/api/store_object/"
        
        self.vc_unsigned = None
        self.vc_signed = None
        
        #Create vc
        self.__create_vc(cred_dict)
        #Sign vc
        self.__sign_vc()
        #Expose cred
        self.__expose_cred(cred_dict)
        #Expose vc
        self.__expose_vc()
            
    def get_id(self):
        return self.vc_id
    def get_did(self):
        return self.vc_did
    def get_vc_unsigned(self):
        return self.vc_unsigned
    def get_vc_signed(self):
        return self.vc_signed

    def __create_vc(self, cred_dict: dict):
        cred_type = cred_dict.get('@type', cred_dict.get('type'))
        
        self.vc_unsigned = {
            "@context": [
                "https://www.w3.org/2018/credentials/v1"
            ],
            "@id": f"{self.vc_did}",
            "@type": [
                f"{self.vc_type}",
                f"{cred_type}",
            ],
            "issuer": f"{self.issuer.get_did()}",
            "credentialSubject": cred_dict
        }
        
        return self.vc_unsigned
    
    def __sign_vc(self):
        headers = {
            "Content-Type": "application/json; charset=UTF-8",
            "Accept": "application/json, application/javascript, text/javascript, text/json"
        }
        response = requests.put(self.sign_api, data=json.dumps(self.vc_unsigned), headers=headers)
        if response.status_code == 200:
            self.vc_signed = json.loads(response.text)
            return self.vc_signed
        else:
            print(f"Error HTTP Code {response.status_code} with {self.sign_api} for {self.vc_agent_type} : {response.text}")
            return None

    def __expose_cred(self, cred_dict: dict):
        cred_did = cred_dict.get("@id", cred_dict.get("id"))
        cred_agent_type, cred_id = extract_type_n_id_from_did(cred_did)
        
        if LOCAL_ENV:
            # Local storage
            print(f'Store {cred_agent_type} with did {cred_did} on local storage')
            os.makedirs(f'./gen/{cred_agent_type}/{cred_id}', exist_ok=True)
            with open(f'./gen/{cred_agent_type}/{cred_id}/data.json', 'w') as f:
                json.dump(obj=cred_dict, fp=f, indent=4)
        else:
            # User agent storage
            print(f'Store {cred_agent_type} with did {cred_did} on {self.store_api}')
            headers = get_header_with_api_key()
            js = {'objectjson': json.dumps(cred_dict)}
            response = requests.post(self.store_api + cred_agent_type, headers=headers, json=js)
            print(f'{response.status_code}: {response.text}')

    def __expose_vc(self):
        if LOCAL_ENV:
            # Local storage
            print(f'Store {self.vc_agent_type} with did {self.vc_did} on local storage')
            os.makedirs(f'./gen/{self.vc_agent_type}/{self.vc_id}', exist_ok=True)
            with open(f'./gen/{self.vc_agent_type}/{self.vc_id}/data.json', 'w') as f:
                json.dump(obj=self.vc_signed, fp=f, indent=4)
        else:
            # User agent storage
            print(f'Store {self.vc_agent_type} with did {self.vc_did} on {self.store_api}')
            headers = get_header_with_api_key()
            js = {'objectjson': json.dumps(self.vc_signed)}
            response = requests.post(self.store_api + self.vc_agent_type, headers=headers, json=js)
            print(f'{response.status_code}: {response.text}')
            

class ComplianceCertificateClaim(AbstractVerifiableCredential):
    def __init__(self, issuer: Participant, scheme_did: str, so_did: str) -> None:
        self.cred_agent_type = 'compliance-certificate-claim'
        self.cred_type = ''.join([x.capitalize() for x in self.cred_agent_type.split('-')])
        
        self.cred_id = rand_id()
        self.cred_did = f"{issuer.get_did()}:participant:{issuer.get_id()}/{self.cred_agent_type}/{self.cred_id}/data.json"
        
        self.credential_subject = {
            "@context": {
                "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                "gx-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
                "gx-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
            },
            "@id": f"{self.cred_did}",
            "@type": f"{self.cred_type}",
            "gx-compliance:hasComplianceCertificationScheme": {
                "@type": "gx-compliance:ComplianceCertificationScheme",
                "@value": f"{scheme_did}"
            },
            "gx-compliance:hasServiceOffering": {
                "@type": "gx-service:ServiceOffering",
                "@value": f"{so_did}"
            }
        }
        
        super().__init__(issuer, self.credential_subject)
        

class ComplianceCertificateCredential(AbstractVerifiableCredential):
    def __init__(self, issuer: Participant, claim_did) -> None:
        self.cred_agent_type = 'compliance-certificate-credential'
        self.cred_type = ''.join([x.capitalize() for x in self.cred_agent_type.split('-')])
        
        self.cred_id = rand_id()
        self.cred_did = f"{issuer.get_did()}:participant:{issuer.get_id()}/{self.cred_agent_type}/{self.cred_id}/data.json"
        
        self.credential_subject = {
            "@context": {
                "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                "gx-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
            },
            "@id": f"{self.cred_did}",
            "@type": f"{self.cred_type}",
            "gx-compliance:credentialSubject": {
                "@type": f"gx-compliance:ComplianceCertificateClaim",
                "@value": f"{claim_did}"
            },
            "gx-compliance:isValid": {
                "@value": "True"
            }
        }
        
        super().__init__(issuer, self.credential_subject)


class ThirdPartyComplianceCertificateClaim(AbstractVerifiableCredential):
    def __init__(self, issuer: Participant, scheme_did, so_did, cab_did) -> None:
        self.cred_agent_type = 'third-party-compliance-certificate-claim'
        self.cred_type = ''.join([x.capitalize() for x in self.cred_agent_type.split('-')])
        
        self.cred_id = rand_id()
        self.cred_did = f"{issuer.get_did()}:participant:{issuer.get_id()}/{self.cred_agent_type}/{self.cred_id}/data.json"
        
        self.credential_subject = {
            "@context": {
                "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                "gx-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
                "gx-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
            },
            "@id": f"{self.cred_did}",
            "@type": f"{self.cred_type}",
            "gx-compliance:hasComplianceCertificationScheme": {
                "@type": "gx-compliance:ComplianceCertificationScheme",
                "@value": f"{scheme_did}"
            },
            "gx-compliance:hasServiceOffering": {
                "@type": "gx-service:ServiceOffering",
                "@value": f"{so_did}"
            },
            "gx-compliance:hasComplianceAssessmentBody": {
                "@type": "gx-participant:ComplianceAssessmentBody",
                "@value": f"{cab_did}"
            }
        }
        
        super().__init__(issuer, self.credential_subject)
        

class ThirdPartyComplianceCertificateCredential(AbstractVerifiableCredential):
    def __init__(self, issuer: Participant, claim_did) -> None:
        self.cred_agent_type = 'third-party-compliance-certificate-credential'
        self.cred_type = ''.join([x.capitalize() for x in self.cred_agent_type.split('-')])
        
        self.cred_id = rand_id()
        self.cred_did = f"{issuer.get_did()}:participant:{issuer.get_id()}/{self.cred_agent_type}/{self.cred_id}/data.json"
        
        self.credential_subject = {
            "@context": {
                "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                "gx-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
            },
            "@id": f"{self.cred_did}",
            "@type": f"{self.cred_type}",
            "gx-compliance:credentialSubject": {
                "@type": "gx-compliance:ThirdPartyComplianceCertificateClaim",
                "@value": f"{claim_did}"
            },
            "gx-compliance:isValid": {
                "@value": "True"
            }
        }
        
        super().__init__(issuer, self.credential_subject)


class SelfAssessedComplianceCriteriaClaim(AbstractVerifiableCredential):
    def __init__(self, issuer: Participant, criterions, so_did) -> None:
        self.cred_agent_type = 'self-assessed-compliance-criteria-claim'
        self.cred_type = ''.join([x.capitalize() for x in self.cred_agent_type.split('-')])
        
        self.cred_id = rand_id()
        self.cred_did = f"{issuer.get_did()}:participant:{issuer.get_id()}/{self.cred_agent_type}/{self.cred_id}/data.json"
        
        self.credential_subject = {
            "@context": {
                "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#"
            },
            "@id": f"{self.cred_did}",
            "@type": f"{self.cred_type}",
            "gx-compliance:hasServiceOffering": so_did,
            "gx-compliance:hasAssessedComplianceCriteria": []
        }
        
        for criterion in criterions:
            self.credential_subject.get('credentialSubject').get('gx-compliance:hasAssessedComplianceCriteria').append(
                {
                    "@type": "gx-compliance:ComplianceCriterion",
                    "@id": f"{criterion}"
                }
            )
            
        super().__init__(issuer, self.credential_subject)


class SelfAssessedComplianceCriteriaCredential(AbstractVerifiableCredential):
    def __init__(self, issuer: Participant, selfclaim_did) -> None:
        self.cred_agent_type = 'self-assessed-compliance-criteria-credential'
        self.cred_type = ''.join([x.capitalize() for x in self.cred_agent_type.split('-')])
        
        self.cred_id = rand_id()
        self.cred_did = f"{issuer.get_did()}:participant:{issuer.get_id()}/{self.cred_agent_type}/{self.cred_id}/data.json"
        
        self.credential_subject = {
            "@context": {
                "gx-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
                "gx-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
            },
            "@id": f"{self.cred_did}",
            "@type": f"{self.cred_type}",
            "gx-compliance:credentialSubject": {
                "@type": "gx-compliance:SelfAssessedComplianceCriteriaCredential",
                "@value": f"{selfclaim_did}"
            },
            "gx-compliance:isValid": {
                "@value": "True"
            }
        }
        
        super().__init__(issuer, self.credential_subject)


def rand_id():
    return hashlib.sha256(bytes(''.join(random.choices(string.ascii_lowercase, k=10)), 'utf-8')).hexdigest()

def get_header_with_api_key():    
    #b64_key = API_KEY_AUTHORIZED
    #key = base64.standard_b64decode(b64_key).decode('UTF-8').replace('\n','')
    key = API_KEY_AUTHORIZED
    return {'x-api-key': key}

def is_credential(js):
    if js.get('credentialSubject'):
        return True
    return False

def extract_info_from_did(did):
    #did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certification-scheme/0c8f60212eb57bb4d64e28831c36acd0c691bcfe52525c85780ffe65e1a1bf30/data.json
    #did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json
    reg = r"did:web:(.*?)\.(provider|auditor)?"
    name, role = re.search(reg, did).groups()
    
    return f"{name}.{role}" if role else f"{name}"
    
def extract_name_from_did(did):
    #did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certification-scheme/0c8f60212eb57bb4d64e28831c36acd0c691bcfe52525c85780ffe65e1a1bf30/data.json
    #did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json
    reg = r"did:web:(.*?)\."
    name = re.search(reg, did).group(1)
    
    return name

def extract_id_from_did(did):
    #did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certification-scheme/0c8f60212eb57bb4d64e28831c36acd0c691bcfe52525c85780ffe65e1a1bf30/data.json
    #did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json
    reg = r".([0-9a-f]{64})/data.json"
    id = re.search(reg, did).group(1)
    
    return id

def extract_type_n_id_from_did(did):
    #did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certification-scheme/0c8f60212eb57bb4d64e28831c36acd0c691bcfe52525c85780ffe65e1a1bf30/data.json
    reg = r"/(.*?).([0-9a-f]{64})/data.json"
    type, id = re.search(reg, did).groups()
    
    return type, id