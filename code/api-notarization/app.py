from flask import Flask, jsonify
from flask_cors import CORS
from flask_swagger import swagger
from config import *
from apis.v2206 import api as api_v2206
from apis.v2210 import api as api_v2210
from apis.vdev import api as api_vdev
from werkzeug.utils import *

# Define app.
app = Flask(__name__)
CORS(app)

if __name__ == "__main__":
	# Register api versions
	app.register_blueprint(api_vdev, url_prefix='/api/vdev/', name="vdev")
	app.register_blueprint(api_v2210, url_prefix='/api/v2210/', name="v2210")
	app.register_blueprint(api_v2206, url_prefix='/api/v2206/', name="v2206")

	# Register current api version
	app.register_blueprint(api_vdev, url_prefix='/api/', name="vcurrent")
	
	@app.route("/spec")
	def spec():
		swag = swagger(app)
		swag['info']['version'] = "1.0"
		swag['info']['title'] = "My API"
		return jsonify(swag)
 
	# Run app
	app.run(host=FLASK_HOST, port=FLASK_PORT, debug=FLASK_DEBUG)