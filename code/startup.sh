#!/bin/sh
set -e

#Start dss-notarization server on port 8080
/bin/sh /dss/apache-tomcat-*/bin/startup.sh
#tail -f /dss/apache-tomcat-*/logs/catalina.out

#Start api-notarization server on port 5000
python3 /api/app.py
