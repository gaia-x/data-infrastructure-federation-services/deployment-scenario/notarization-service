# Notarizarion Service
## Description

Notarization Service is composed of two services :
 - dss-notarization-demo which is a modified fork of the european project called [DSS Web Application](https://github.com/esig/dss-demonstrations#dss-web-application) v5.11, which provides some useful signature functions
 - dss-notarization-api which is a back-end app developped to provide VC for some certifications (signed PDF) that does not provide VC.

## Getting started
### Entrypoint
 - dss-notarization-demo can be reached as a website on this address : https://notarization-service.abc-federation.gaia-x.community/, but it exposed some webservices, specificaly REST and SOAP : https://notarization-service.abc-federation.gaia-x.community/services/
 - dss-notarization-api can be reached as an API on this address : https://notarization-service.abc-federation.gaia-x.community/api/

It is important to notice that only dss-notarization-api called webservices of dss-notarization-demo, since all signature functions is deliver by this components and only used by dss-notarization-api.

Only dss-notarization-api is reachable by GXFS components, especialy wizard tool.

### Technology
 - dss-notarization-demo is a Spring Boot Application.
 - dss-notarization-api is a simple Flask API.

### Stack
//

### Installation and usage
 - dss-notarizarion-demo : a makefile and two dockerfiles are given as example.
 - dss-notarizarion-api : same thing

> Tips to python project
```
#Creation of a python virtual environment
./setup.sh 

#Enable it
source venv/bin/activate

#Pull dependances
pip install -r requirements.txt

#Launch app
./startup.sh
```
## Notarization Journey
As mentioned before, the aim of the notarization service is to issue VCs of certifications on behalf of auditors or certification issuers who are not yet able to issue VCs. Since auditors or certifications providers do not yet issue certifications in VC format properly interpreted in gaia-x, Notarization Service will be called to transform current signed PDF certification documents into VCs.

We can therefore summarize the role of notarization in a component that makes it possible to transform signed pdf documents of proof of certification into VC.

### Trusted List
To be able to verify pdf signatures, the Notarization Service uses dss-notarization-demo to update its european trusted lists : 
 - European Trusted List : https://ec.europa.eu/tools/lotl/eu-lotl.xml
 - EU Official Journal Trusted Certificates : https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.C_.2019.276.01.0001.01.ENG
 - Trusted Certificates of Trusted Registry of the Federation : https://trusted-registry.abc-federation.gaia-x.community/api/v1/certs

All these credential are supposed to be trusted, trusted list for certification and trusted registry for self assessment.

Any signatures can be verified with this service exposed by the dss-notarization-demo and used by dss-notarization-api when it receives some base 64 encoded string related to pdf document pdf file.

### VC Creation (certification)
First step is the creation of the VC LocatedServiceOffering asked by a service provider.

After having filled out service description for a given service, wizard tool is supposed to call Notarization. It calls it when it drag and drop its signed pdf related to his certification.

For instance, first certification have to call ***/create-vc/*** routes on notarization with a json object as argument including name of the participant, certification type, base64 encoded string of the pdf document and the *ServiceOffering* object being used. 
```
-> output
POST https://notarization-service.abc-federation.gaia-x.community/api/create-vc
ARG json object
{
	"participant": "dufoustorage",
	"certificationType": "ISO 27001",
	"b64file": "123456",
	"serviceOffering": ServiceOffering_JsonObject,
}

<- input
{
	"result": "OK/NOK", 
	"message": string, 
	"vc": LocatedServiceOffering_VC (signed)
}
```
During this step Notarization has to create and use some ontology objects such as :
 - ComplianceReference related to certification,
 - ThirdPartyComplianceCertificationScheme
 - ComplianceAssessmentBody
 - ThirdPartyComplianceCertificateClaim
 - ThirdPartyComplianceCertificateCredential
 - LocatedServiceOffering

### VC Creation (self assessment)
Some criteria related to label attribution can be self assessed, that's why user might fill out a questionnaire on wizard tool. Answers must have to be concatenate to the VC LocatedServiceOffering previously issued (an update need te be made by notarization service).  
V2206
```
-> ie. questionnaire answers
[
    {
        "section": "Contractual governance",
        "criterion": "Criterion 1: The provider shall offer the ability to establish a legally binding act. This legally binding act shall documented",
        "val": true,
        "relevantFor": [
            "1",
            "2",
            "3"
        ]
    },
    ...
    {
        "section": "European Control",
        "criterion": "Criterion 61: The provider shall not access customer data unless authorised by the customer or when the access is in accordance with EU/EEA/member state law",
        "val": false,
        "relevantFor": [
            "1",
            "2",
            "3"
        ]

    }
]
Last steps deal with the creation and usage of some ontology objects such as :
 - SelfAssessedComplianceCriteriaClaim
 - SelfAssessedComplianceCriteriaCredential
 - LocatedServiceOffering

```

V2210
```
-> ie. questionnaire answers
[
    {
        "category": "Contractual governance",
        "name": "Criterion 1: The provider shall offer the ability to establish a legally binding act. This legally binding act shall documented",
        "value": true
    },
    ...
    {
        "category": "European Control",
        "name": "Criterion 61: The provider shall not access customer data unless authorised by the customer or when the access is in accordance with EU/EEA/member state law",
        "value": false
    }
]
Last steps deal with the creation and usage of some ontology objects such as :
 - SelfAssessedComplianceCriteriaClaim
 - SelfAssessedComplianceCriteriaCredential
 - LocatedServiceOffering

```
### VP Creation
Finaly a VP is created and it includes all VCs mandatory to service provider in order to have compliance and label. It's done through a call to the notarization route **/create-vp/**. 

## Authors and acknowledgment
GXFS-FR

## License
The Notarizarion Service (developped for the Gaia-X Federated Services Summit 2022 Demonstration) except dss-notarization-demo is delivered under the terms of the Apache License Version 2.0.

As the dss-notarization-demo is a fork of the european project called DSS, the code is delivered under its license given at the root of the sub-project.